﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System;

namespace DAPP.ERiskCommon
{
    public class SessionExpire : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            // if (HttpContext.Current.Session["Username"] == null || HttpContext.Current.Session["Token"] == null)
            if (Convert.ToString(filterContext.HttpContext.Session.GetString("Username")) == "" || Convert.ToString(filterContext.HttpContext.Session.GetString("Token")) == "")
            {
                //FormsAuthentication.SignOut();
                filterContext.Result =
               new RedirectToRouteResult(new RouteValueDictionary
                {
                        { "action", "DAppLogin" },
                        { "controller", "Account" },
                        {"Area",String.Empty}
                        //,{ "returnUrl", filterContext.HttpContext.Request.RawUrl}  
                 });

                return;
            }
        }
    }
}
