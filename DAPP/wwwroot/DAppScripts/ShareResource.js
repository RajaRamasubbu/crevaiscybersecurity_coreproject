﻿var urlAllias = window.location.href.split('/')[3];

$(document).ready(function () {
    $('#LoadingPanel').addClass('dvLoading');
    document.getElementById("PageHeader").innerHTML = "e-Risk Resource Pool - Share Resource";
    // debugger;
    $('.active').removeClass('active');
    $('#ShareResourceId').addClass('active');
    $('.treeview').addClass('active');
    //ShareResourceGrid();
    $('#LoadingPanel').removeClass('dvLoading');

});

$(function () {
    //debugger;
    $('#ProcessingId').show();
    $('#StorageId').hide();
    $('#ShareResourceGrid').show();
    $("input[name='radiobutton']").click(function () {
        //debugger;
        if ($("#ProcessingCheckbox").is(":checked")) {
            $('#ProcessingId').show();
            $('#StorageId').hide();
        } else {
            $('#ProcessingId').hide();
            $('#StorageId').show();
            // $('input[type="hidden"]').each(function () { this.type = 'text'; });
        }
    });
});

function DevicesDetails(ResourceType) {
    //debugger;
    var _DeviceId;
    if (ResourceType == 'RAM')
        _DeviceId = $("#DevicesLst").val();
    else if (ResourceType == 'Storage')
        _DeviceId = $("#StoDevicesLst").val();
    var URL = "";
    if (urlAllias == 'ResourcePool') {
        URL = '/ResourcePool/GetTotalResourceAvailability';
    }
    else {
        URL = '/' + urlAllias + '/ResourcePool/GetTotalResourceAvailability';
    }

    //if (_DeviceId == "258") {
        $.ajax({
            url: URL,
            type: "GET",
            data: { ResourceType: ResourceType, DeviceId: _DeviceId },
            contentType: "application/json",
            success: function (response, status, xhr) {
                if (ResourceType == 'RAM') {
                    document.getElementById("TotalprocessingpowerId").value = response;
                    document.getElementById("ShareProcessingPercentageId").value = "";
                    document.getElementById("ShareProcessingUnitId").value = "";
                }
                else if (ResourceType == 'Storage') {
                    document.getElementById("TotalStorageId").value = response;
                    document.getElementById("ShareStoragePerId").value = "";
                    document.getElementById("ShareStorageUnitId").value = "";
                }
                    
            }, error: function () {
            }
        });
    //} else {
    //    document.getElementById("DevicesLst").value = "";
    //    document.getElementById("StoDevicesLst").value = "";
    //    bootbox.alert("Please configure device into shell scripts before sharing.");
    //}
}

function ValidateSharedDevice(ResourceType) {

    //debugger
    var _SharedFromDeviceId="";
    var _SharedToDeviceId = "";
    if (ResourceType == 'RAM') {
        _SharedFromDeviceId = $("#DevicesLst").val();
        _SharedToDeviceId = $("#ProcessingSharedToDeviceId").val();
    }
    else if (ResourceType == 'Storage') {
        _SharedFromDeviceId = $("#StoDevicesLst").val();
        _SharedToDeviceId = $("#StoSharedToDeviceId").val();
    }

    if (_SharedFromDeviceId == "" || _SharedFromDeviceId == null) {
        document.getElementById("ProcessingSharedToDeviceId").value = "";
        document.getElementById("StoSharedToDeviceId").value = "";
        bootbox.alert("Please select Share Resource From device first before selecting Share Resource To device.");
    }
    else if (_SharedFromDeviceId == _SharedToDeviceId) {
        document.getElementById("ProcessingSharedToDeviceId").value = "";
        document.getElementById("StoSharedToDeviceId").value = "";
        bootbox.alert("Share Resource From and Share Resource To device cannot be same, Please select different Share Resource To deivce.");
    }


}

function CalProcessingUnit() {
    //debugger;
    var TotalPower = $("#TotalprocessingpowerId").val();
    var SharePowerPer = $("#ShareProcessingPercentageId").val();
    if (SharePowerPer < 0 || SharePowerPer > 100) {
        document.getElementById("ValidationMessages").innerHTML = "Percentage value must be between 0 and 100";
    }
    else {
        var RAMShared = (TotalPower / 100) * SharePowerPer;
        document.getElementById("ShareProcessingUnitId").value = RAMShared.toFixed(2);
    }
}

function ResetProcessing() {
    document.getElementById("ShareProcessingPercentageId").value = "";
    document.getElementById("ShareProcessingUnitId").value = "";
}


function ShareResourceSave(ResourceType) {
    $('#LoadingPanel').addClass('dvLoading');
    //debugger
    var SaveValidation = "";
    var _DeviceId = ""; var TotalAvailabity = ""; var SharedResourcePercentage = ""; var SharedResource = "";
    var _SharedToDeviceid = "";
    if (ResourceType == "RAM") {
        _DeviceId = $("#DevicesLst").val();
        TotalAvailabity = $("#TotalprocessingpowerId").val();
        SharedResourcePercentage = $("#ShareProcessingPercentageId").val();
        _SharedToDeviceid = $("#ProcessingSharedToDeviceId").val();
        SharedResource = $("#ShareProcessingUnitId").val();

        SaveValidation = SaveProcessingValidation(_DeviceId, SharedResourcePercentage, _SharedToDeviceid);

    } else if (ResourceType == "Storage") {
        _DeviceId = $("#StoDevicesLst").val();
        TotalAvailabity = $("#TotalStorageId").val();
        SharedResourcePercentage = $("#ShareStoragePerId").val();
        SharedResource = $("#ShareStorageUnitId").val();
        _SharedToDeviceid = $("#StoSharedToDeviceId").val();

        SaveValidation = SaveStoValidation(_DeviceId, SharedResourcePercentage, _SharedToDeviceid);
    }


    if (SaveValidation == true && TotalAvailabity > 0) {
        var URL = "";
        if (urlAllias == 'ResourcePool') {
            URL = '/ResourcePool/ShareResourceProcessing';
        }
        else {
            URL = '/' + urlAllias + '/ResourcePool/ShareResourceProcessing';
        }
        $.ajax({
            url: URL,
            type: "POST",
            data: { DeviceId: _DeviceId, TotalAvailabity: TotalAvailabity, SharedResource: SharedResource, ResourceType: ResourceType, ShareToDeviceId: _SharedToDeviceid },
            dataType: "text",
            success: function (response) {
                //debugger;
                if (response == "true") {
                    //SaveSessionvalues(_DeviceId, TotalAvailabity, SharedResource, ResourceType);
                    if (ResourceType == "RAM") {
                        document.getElementById("DevicesLst").value = "";
                        document.getElementById("TotalprocessingpowerId").value = 0;
                        document.getElementById("ShareProcessingPercentageId").value = 0;
                        document.getElementById("ShareProcessingUnitId").value = 0;
                    }
                    else if (ResourceType == "Storage") {
                        document.getElementById("StoDevicesLst").value = "";
                        document.getElementById("TotalStorageId").value = "";
                        document.getElementById("ShareStoragePerId").value = "";
                        document.getElementById("ShareStorageUnitId").value = "";
                    }
                    //debugger;
                    //ShareResourceGrid();
                    $('#LoadingPanel').removeClass('dvLoading');
                    bootbox.alert("Resource sharing started Successfully.", function () { location.reload(true); });
                    //location.reload(true);
                }
            },
            error: function () {
                $('#LoadingPanel').removeClass('dvLoading');
                bootbox.alert("Cannot able to start Resource sharing Successfully, Please try again.", function () { location.reload(true); });
            }
        });
    }
    else {
        $('#LoadingPanel').removeClass('dvLoading');
        if (TotalAvailabity <= 0) {
            bootbox.alert("Cannot share the resource, as the total availability of Processing/Storage is zero.");
        }
        else if (_SharedToDeviceid == "" || _SharedToDeviceid == null) {
            bootbox.alert("Please select Share Resource To device and try to save.");
        }
    }
}

function SaveProcessingValidation(_DeviceId, SharedResourcePercentage) {
    //debugger
    if (_DeviceId == "" || _DeviceId == null || _SharedToDeviceid == "" || _SharedToDeviceid == null) {
        if (_DeviceId == "" || _DeviceId == null)
            document.getElementById("ProDevName").innerHTML = "*";
        else
            document.getElementById("prosharedToDev").innerHTML = "*";
        return false;
    } else if (SharedResourcePercentage == "" || SharedResourcePercentage == null || SharedResourcePercentage == 0) {
        document.getElementById("ValidationMessages").innerHTML = "Percentage value must be between 0 and 100";
        document.getElementById("ProDevName").innerHTML = "";
        return false;
    } else {
        document.getElementById("ProDevName").innerHTML = "";
        document.getElementById("ValidationMessages").innerHTML = "";
        return true;
    }
}


//---------------------------------Storage----------------------------------------


function ResetStorage() {
    document.getElementById("ShareStoragePerId").value = "";
    document.getElementById("ShareStorageUnitId").value = "";
}

//function ShareResourceGrid() {
//    //debugger;
//    var URL = "";
//    if (urlAllias == 'ResourcePool') {
//        URL = '/ResourcePool/ShareResourceGrid';
//    }
//    else {
//        URL = '/' + urlAllias + '/ResourcePool/ShareResourceGrid';
//    }
//    $.ajax({
//        url: URL,
//        type: "GET",
//        data: {},
//        contentType: "application/json",
//        success: function (response, status, xhr) {
//            var ct = xhr.getResponseHeader("content-type") || "";
//            if (ct.indexOf('html') > -1) {
//                $("#ShareResourceGrid").html(response);
//            }
//        }, error: function () {
//        }
//    });
//}

function SaveStoValidation(_DeviceId, SharedResourcePercentage, _SharedToDeviceid) {
    //debugger
    if (_DeviceId == "" || _DeviceId == null || _SharedToDeviceid == "" || _SharedToDeviceid == null) {
        if (_DeviceId == "" || _DeviceId == null)
            document.getElementById("StoDevName").innerHTML = "*";
        else
            document.getElementById("StoSharedToDevName").innerHTML = "*";
        return false;
    } else if (SharedResourcePercentage == "" || SharedResourcePercentage == null || SharedResourcePercentage == 0) {
        document.getElementById("StoValidationMessages").innerHTML = "Percentage value must be between 0 and 100";
        document.getElementById("StoDevName").innerHTML = "";
        return false;
    } else {
        document.getElementById("StoDevName").innerHTML = "";
        document.getElementById("StoValidationMessages").innerHTML = "";
        return true;
    }
}

//function SaveSessionvalues(DeviceName, TotalAvailabity, SharedResource, ResourceType) {
//    debugger;
//    var URL = "";
//    if (urlAllias == 'ResourcePool') {
//        URL = '/ResourcePool/UpdateShareResourceSession';
//    }
//    else {
//        URL = '/' + urlAllias + '/ResourcePool/UpdateShareResourceSession';
//    }
//    $.ajax({
//        url: URL,
//        type: "POST",
//        data: { DeviceId: DeviceName, TotalAvailabity: TotalAvailabity, SharedResource: SharedResource, ResourceType: ResourceType },
//        dataType: "text",
//        success: function (response) {
//        }
//    });
//}

function CalStorageUnit() {
    //debugger;
    var TotalStorage = $("#TotalStorageId").val();
    var ShareStoragePer = $("#ShareStoragePerId").val();
    if (ShareStoragePer < 0 || ShareStoragePer > 100) {
        document.getElementById("StoValidationMessages").innerHTML = "Percentage value must be between 0 and 100";
    }
    else {
        var StorageShared = (TotalStorage / 100) * ShareStoragePer;
        document.getElementById("ShareStorageUnitId").value = StorageShared.toFixed(2);
    }
}

function StopResourceSharing(Id) {
    // debugger;
    bootbox.confirm("Are you sure, you want to stop the resource sharing? Entire resource sharing will be stopped.",
        function (result) {
            if (result == true) {
                $('#LoadingPanel').addClass('dvLoading');
                var URL = "";
                if (urlAllias == 'ResourcePool') {
                    URL = '/ResourcePool/StopResourceSharing';
                }
                else {
                    URL = '/' + urlAllias + '/ResourcePool/StopResourceSharing';
                }
                $.ajax({
                    url: URL,
                    type: "POST",
                    data: { ShareResourceId: Id },
                    dataType: "text",
                    success: function (response) {
                        $('#LoadingPanel').removeClass('dvLoading');
                        if (response == "true") {
                            bootbox.alert("Resource Sharing Stopped Successfully.", function () { location.reload(true); });
                        }
                        else {
                            bootbox.alert("Failed to stop resource sharing, Please try again.", function () { location.reload(true); });
                        }
                    }

                });
            }
        }
    );
}