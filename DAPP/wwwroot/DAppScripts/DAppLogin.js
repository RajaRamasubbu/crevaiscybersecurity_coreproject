﻿
var Baseurl = window.location.href;
function Login() {
    debugger;
    var username = $("#Customerid").val();
    var password = $("#Passwordval").val();
    var RememberMe = $("#IsRememberId").val();
    var URL = $('#APIURL').val();
    var LoginVal = LoginValidation(username, password, URL);
    if (LoginVal == true) {
        $('#LoadingPanel').addClass('dvLoading');
        try {
            //debugger
            var data = { username: username.toString(), password: password.toString(), apiCall: "1" }
            var lastChar = URL[URL.length - 1];
            var LoginURL = "";
            if (lastChar == '/')
                LoginURL = URL + "api/authentication/organizations";
            else
                LoginURL = URL + "/api/authentication/organizations";

            $.ajax({
                type: "POST",
                //url: "https://ec2-34-215-146-109.us-west-2.compute.amazonaws.com:3000/api/authentication/organizations",
                url: LoginURL,
                data: JSON.stringify(data),
                dataType: "json",
                contentType: "application/json",
                success: function (response) {
                    var Token = Object.values(response);
                    UpdateUserNameSession(username, Token);
                    GetDeviceDetails(username, Token, URL);
                    //debugger;
                    if (RememberMe == "true") {
                        SetCookies(username, password, URL);
                    }
                    $('#LoadingPanel').removeClass('dvLoading');
                }, error: function () {
                    document.getElementById("CredentialError").innerHTML = "Invalid Credentials";
                    $('#LoadingPanel').removeClass('dvLoading');
                    return false;
                }
            });
        }
        catch (err) {
            document.getElementById("CredentialError").innerHTML = "Invalid Credentials";
            $('#LoadingPanel').removeClass('dvLoading');
        }
    }
    else {
        return;
    }
};
//});

function UpdateUserNameSession(username, Token) {
    //debugger;
    if (Baseurl.split('/')[3] != "") {
        Baseurl = Baseurl + '/';
    }
    //var URL = Baseurl + 'Account/UpdateUserNameSession';
    $.ajax({
        url: Baseurl + 'Account/UpdateUserNameSession',
        type: "POST",
        data: { Username: username.toString(), Token: Token.toString() },
        dataType: "text",
        success: function (response) {
            MyDevices();
        }
    });
}

function GetDeviceDetails(username, Token, URL) {
    var UserName = username;
    var TokenVal = Token;
    console.log(TokenVal);
    var LoginURL = "";
    var lastChar = URL[URL.length - 1];
    if (lastChar == '/')
        LoginURL = URL + "api/organizations/agents/?username=" + UserName;
    else
        LoginURL = URL + "/api/organizations/agents/?username=" + UserName;

    $.ajax
        ({
            type: "GET",
            url: LoginURL,//"https://ec2-34-215-146-109.us-west-2.compute.amazonaws.com:3000/api/organizations/agents/?username=" + UserName,
            contentType: "application/json",
            dataType: 'json',
            async: false,
            data: '{}',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + TokenVal);
            },
            success: function (response) {
                //debugger;

                if (typeof (response["name"]) == "string") {
                    //debugger
                    UpdateDevicesDB(UserName.toString(), response);
                }
                else {
                    //debugger
                    alert(response["error"]);
                }
            }, error: function () {
                alert("Login Failed");
            }
        });
}

function UpdateDevicesDB(UserName, response) {
    //debugger;
    if (Baseurl.split('/')[3] != "") {
        Baseurl = Baseurl + '/';
    }
    $.ajax({
        url: Baseurl + 'Mydevices/UpdateDeviceDetails',
        type: "POST",
        data: { Username: UserName.toString(), Users: JSON.stringify(response) },
        dataType: "json",
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json;charset=UTF-8");
            }
        },
        success: function (data) {
            //alert(data)
        },
        error: function (data) {
            // alert(data);
        }


    });
}
function LoginValidation(username, password, URL) {
    //debugger;
    if (username.toString() == null || username.toString() == "" || password.toString() == null || password.toString() == "" || URL.toString() == null || URL.toString() == "") {
        if (username.toString() == null || username.toString() == "") {
            document.getElementById("CustomerIdError").innerHTML = "Customer Id is required";
        } else { document.getElementById("CustomerIdError").innerHTML = ""; }

        if (password.toString() == null || password.toString() == "") {
            document.getElementById("PasswordError").innerHTML = "Password is required";
        } else { document.getElementById("PasswordError").innerHTML = ""; }

        if (URL.toString() == null || URL.toString() == "") {
            document.getElementById("URLError").innerHTML = "URL is required";
        } else { document.getElementById("URLError").innerHTML = ""; }

        return false;
    }
    else {
        document.getElementById("CustomerIdError").innerHTML = "";
        document.getElementById("PasswordError").innerHTML = "";
        document.getElementById("URLError").innerHTML = "";
        return true;
    }
}

function SetCookies(username, password, URL) {
    $.ajax({
        url: Baseurl + 'Account/SetCookies',
        type: "POST",
        data: { CustomerId: username.toString(), Password: password.toString(), URL: URL.toString() },
        dataType: "text",
        success: function (response) {
        }
    });
    //debugger
}

function MyDevices() {
    //debugger
    if (Baseurl.split('/')[3] != "") {
        Baseurl = Baseurl + '/';
    }
    location.href = Baseurl + 'Mydevices/DeviceDetails';
}


