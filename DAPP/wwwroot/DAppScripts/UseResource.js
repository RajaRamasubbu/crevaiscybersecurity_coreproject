﻿var urlAllias = window.location.href.split('/')[3];
$(document).ready(function () {
    document.getElementById("PageHeader").innerHTML = "e-Risk Resource Pool - Use Resource";
    $('.active').removeClass('active');
    $('#UseResourceId').addClass('active');
    $('.treeview').addClass('active');
});

$(function () {
    //debugger;
    $('#ProcessingId').show();
    $('#StorageId').hide();
    $("input[name='radiobutton']").click(function () {
        //debugger;
        if ($("#ProcessingCheckbox").is(":checked")) {
            $('#ProcessingId').show();
            $('#StorageId').hide();
        } else {
            $('#ProcessingId').hide();
            $('#StorageId').show();
            $('input[type="hidden"]').each(function () { this.type = 'text'; });
        }
    });
});

function ProcessingReset() {
    //debugger;
    document.getElementById("TotalRequirementId").value = 0;
    document.getElementById("DurationNeededId").value = "00:00:00";
}

function UpdateProcessing()
{
    debugger;
    var _DeviceName = $("#RAMDeviceId").val();
    var TotalRequirement = $("#TotalRequirementId").val();
    var TimeNeeded = $("#DurationNeededId").val();
    var TotalAvailability = $("#AvailableRAMId").val();
    var Validation = SaveProcessinValidation(_DeviceName, TotalRequirement, TimeNeeded, TotalAvailability);
    if (Validation == true)
    {
        var URL = "";
        if (urlAllias == 'ResourcePool') {
            URL = '/ResourcePool/UpdateUseResourceProcessing';
        }
        else {
            URL = '/' + urlAllias + '/ResourcePool/UpdateUseResourceProcessing';
        }
        $.ajax({
            url: URL,
            type: "POST",
            data: { DeviceName: _DeviceName.toString(), RAMRequired: TotalRequirement, RequiredTime: TimeNeeded },
            dataType: "text",
            success: function (response) {
                if (response == "true") {
                    location.reload();
                }
            }, error: function (e) {
                alert(e);
            }
        });
    }
}

function SaveProcessinValidation(DeviceName, TotalRequirement, TimeNeeded, TotalAvailability)
{
    if (DeviceName == (null || "")) {
        document.getElementById("ValidationMessage").innerHTML = "Please Select Device Name from Share Resource screen";
        return false;
     }
    else if (Math.round(TotalAvailability * 100) < Math.round(TotalRequirement * 100)) {
        document.getElementById("ValidationMessage").innerHTML = "Total Requirement must be less that Total Availability";
        return false;
     }
    else if (TimeNeeded == (null || "" || "00:00:00")) {
        document.getElementById("ValidationMessage").innerHTML = "Please mention the Duration Needed for Resource Usage";
        return false;
     }
    else {
        //debugger;
        document.getElementById("ValidationMessage").innerHTML = "";
        return true;
    }
}

function GetResourceDetails()
{
    //debugger;
    var DeviceName = $("#RAMDeviceId").val();
    var URL = "";
    if (urlAllias == 'ResourcePool') {
        URL = '/ResourcePool/GetRAMShared';
    }
    else {
        URL = '/' + urlAllias + '/ResourcePool/GetRAMShared';
    }
    $.ajax({
        url: URL,
        type: "GET",
        data: { DeviceName: DeviceName },
        contentType: "text",
        success: function (response, status, xhr) {
            document.getElementById("AvailableRAMId").value = response;
        }, error: function () {
        }
    });
}
//---------------------------------Storage----------------------------------------

function StorageReset() {
    //debugger;
    document.getElementById("TotalRequirementStorId").value = 0;
    document.getElementById("StoDurationNeededId").value = "00:00:00";
}

function UpdateStorage() {
    debugger;
    var DeviceName = $("#StoDeviceName").val();
    var TotalRequirement = $("#TotalRequirementStorId").val();
    var TimeNeeded = $("#StoDurationNeededId").val();
    var TotalAvailability = $("#AvailableStorageId").val();
    var Validation = SaveStorageValidation(DeviceName, TotalRequirement, TimeNeeded, TotalAvailability);
    if (Validation == true) {
        var URL = "";
        if (urlAllias == 'ResourcePool') {
            URL = '/ResourcePool/UpdateUseResourceStorage';
        }
        else {
            URL = '/' + urlAllias + '/ResourcePool/UpdateUseResourceStorage';
        }
        $.ajax({
            url: URL,
            type: "POST",
            data: { DeviceName: DeviceName.toString(), StorageRequired: TotalRequirement, RequiredTime: TimeNeeded },
            dataType: "text",
            success: function (response, status, xhr) {
                if (response == "True") {
                    location.reload();
                }
            }, error: function () {
            }
        });
    }
}

function SaveStorageValidation(DeviceName, TotalRequirement, TimeNeeded, TotalAvailability) {
    if (DeviceName == (null || "")) {
        document.getElementById("StoValidationMessage").innerHTML = "Please Select Device Name from Share Resource screen";
        return false;
    }
    else if (Math.round(TotalAvailability * 100) < Math.round(TotalRequirement * 100)) {
        document.getElementById("StoValidationMessage").innerHTML = "Total Requirement must be less that Total Availability";
        return false;
    }
    else if (TimeNeeded == (null || "" || "00:00:00")) {
        document.getElementById("StoValidationMessage").innerHTML = "Please mention the Duration Needed for Resource Usage";
        return false;
    }
    else {
        document.getElementById("StoValidationMessage").innerHTML = "";
        return true;
    }
}

function GetStorageShared() {
    var DeviceName = $("#StorageDeviceId").val();
    var URL = "";
    if (urlAllias == 'ResourcePool') {
        URL = '/ResourcePool/GetStorageShared';
    }
    else {
        URL = '/' + urlAllias + '/ResourcePool/GetStorageShared';
    }
    $.ajax({
        url: URL,
        type: "GET",
        data: { DeviceName: DeviceName },
        contentType: "text",
        success: function (response, status, xhr) {
            document.getElementById("AvailableStorageId").value = response;
        }, error: function () {
        }
    });
}