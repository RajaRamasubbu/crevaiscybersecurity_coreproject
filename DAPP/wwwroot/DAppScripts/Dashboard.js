﻿var urlAllias = window.location.href.split('/')[3];
$(document).ready(function () {
    document.getElementById("PageHeader").innerHTML = "e-Risk Resource Pool - Dashboard";
    DeviceData();
    $('.active').removeClass('active');
    $('#DashboardId').addClass('active');
    $('.treeview').addClass('active');
});


function DeviceData() {
    //fetch data from api, on success load below function
    DevicePageRender();

}

function DevicePageRender() {

    var URL = "";
    if (urlAllias == 'ResourcePool') {
        URL = '/ResourcePool/DashBoardPartial';
    }
    else {
        URL = '/' + urlAllias + '/ResourcePool/DashBoardPartial';
    }
    $.ajax({
        url: URL,
        type: 'GET',

        contentType: "application/json",
        success: function (response, status, xhr) {
            var ct = xhr.getResponseHeader("content-type") || "";
            if (ct.indexOf('html') > -1) {
                $("#DeviceAttributes").html(response);
            }
        }, error: function () {
        }
    });
}

function GetDashboardDetails() {
    var _DeviceId = $("#DeviceId").val();
    if (_DeviceId > 0) {
        var URL = "";
        if (urlAllias == 'ResourcePool') {
            URL = '/ResourcePool/DashBoardPartial';
        }
        else {
            URL = '/' + urlAllias + '/ResourcePool/DashBoardPartial';
        }
        $.ajax({
            url: URL,
            type: 'GET',
            data: { DeviceId: _DeviceId },
            contentType: "application/json",
            success: function (response, status, xhr) {
                var ct = xhr.getResponseHeader("content-type") || "";
                if (ct.indexOf('html') > -1) {
                    $("#DeviceAttributes").html(response);
                }
            }, error: function () {
            }
        });
    }
}