#pragma checksum "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\ShareResourceGrid.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "064df0a4db1d6fcc22086f5f971de4af899277ff"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_ResourcePool_ShareResourceGrid), @"mvc.1.0.view", @"/Views/ResourcePool/ShareResourceGrid.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/ResourcePool/ShareResourceGrid.cshtml", typeof(AspNetCore.Views_ResourcePool_ShareResourceGrid))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\_ViewImports.cshtml"
using DAPP;

#line default
#line hidden
#line 2 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\_ViewImports.cshtml"
using DAPP.Models;

#line default
#line hidden
#line 4 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#line 1 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\ShareResourceGrid.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"064df0a4db1d6fcc22086f5f971de4af899277ff", @"/Views/ResourcePool/ShareResourceGrid.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"269350e75962eaa2f635096c18b22f356f66653b", @"/Views/_ViewImports.cshtml")]
    public class Views_ResourcePool_ShareResourceGrid : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(35, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(39, 4693, false);
#line 3 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\ShareResourceGrid.cshtml"
Write(Html.DevExtreme().DataGrid<DAPP_Model.ERiskDApp.ShareResourceGrid>()
                                                        .ID("gridShareddevice")
                                                          .DataSource(d => d
                                                            .Mvc()
                                                            .Controller("DevicedetailsGridAPI")
                                                            .LoadAction("SharedResourceList")
                                                            .Key("Id")
                                                            .LoadParams(new { Username = Context.Session.GetString("Username") })
                                                        )
                                                        .RemoteOperations(true)
                                                        .Columns(columns =>
                                                        {

                                                        columns.AddFor(m => m.DeviceName)
                                                                     .Caption("Share Resource From");

                                                        columns.AddFor(m => m.ResourceType)
                                                                    .Caption("Resource Type");


                                                        columns.AddFor(m => m.TotalAvailability)
                                                                   .Caption("Total Availability");

                                                        columns.AddFor(m => m.ShareResource)
                                                                   .Caption("Shared Storage/Processing");

                                                        columns.AddFor(m => m.ShareStatus)
                                                              .Caption("Is Sharing Stopped")
                                                              .DataType(GridColumnDataType.Boolean);

                                                        columns.Add().DataField("SharedToDeviceName").Caption("Share Resource To")
                                                                     .Alignment(HorizontalAlignment.Left);

                                                        columns.Add().DataField("ShareStatus").Caption("")
                                                                     .CellTemplate(
                                                                        item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    BeginContext(2605, 186, true);
    WriteLiteral("\r\n                                                                            <% if (value != true) { %>\r\n\r\n                                                                            <a");
    EndContext();
    BeginWriteAttribute("href", " href=\"", 2791, "\"", 2831, 3);
#line 41 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\ShareResourceGrid.cshtml"
WriteAttributeValue("", 2798, Url.Action("Edit"), 2798, 19, false);

#line default
#line hidden
    WriteAttributeValue("", 2817, "/<%=", 2817, 4, true);
    WriteAttributeValue(" ", 2821, "data.Id%>", 2822, 10, true);
    EndWriteAttribute();
    BeginContext(2832, 238, true);
    WriteLiteral(" onclick=\"StopResourceSharing(\'<%= data.Id%>\'); return false;\">Stop Sharing</a>\r\n                                                                            <% } %>\r\n                                                                        ");
    EndContext();
    PopWriter();
}
))
                                                                     .Alignment(HorizontalAlignment.Center);

                                                                    })
                                                                    .FilterRow(f => f.Visible(true))
                                                                    .HeaderFilter(f => f.Visible(true))
                                                                    .GroupPanel(p => p.Visible(true))
                                                                    //.Scrolling(s => s.Mode(GridScrollingMode.Virtual))
                                                                    //.Height(500)
                                                                    .ShowBorders(true)
                                                                    .Grouping(g => g.AutoExpandAll(false))
                                                                    .Paging(paging => paging.PageSize(10))
                                                                    .Pager(pager =>
                                                                    {
                                                                        pager.ShowPageSizeSelector(true);
                                                                        pager.AllowedPageSizes(new List<int> { 5, 10, 20 });
                                                                        pager.ShowInfo(true);
                                                                    })
                                                                    .FilterPanel(f => f.Visible(true))

);

#line default
#line hidden
            EndContext();
            BeginContext(4747, 6, true);
            WriteLiteral("\r\n\r\n\r\n");
            EndContext();
            BeginContext(9353, 2, true);
            WriteLiteral("\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
