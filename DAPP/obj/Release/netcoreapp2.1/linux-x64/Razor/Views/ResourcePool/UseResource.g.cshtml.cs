#pragma checksum "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a6f4fd95fe9413b988e84596411ae6ab502eac55"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_ResourcePool_UseResource), @"mvc.1.0.view", @"/Views/ResourcePool/UseResource.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/ResourcePool/UseResource.cshtml", typeof(AspNetCore.Views_ResourcePool_UseResource))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\_ViewImports.cshtml"
using DAPP;

#line default
#line hidden
#line 2 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\_ViewImports.cshtml"
using DAPP.Models;

#line default
#line hidden
#line 4 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a6f4fd95fe9413b988e84596411ae6ab502eac55", @"/Views/ResourcePool/UseResource.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"269350e75962eaa2f635096c18b22f356f66653b", @"/Views/_ViewImports.cshtml")]
    public class Views_ResourcePool_UseResource : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<DAPP_Model.ERiskDApp.UseResourceModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/Content/UseResource.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/DAppScripts/UseResource.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
  
    ViewBag.Title = "Use Resource";

#line default
#line hidden
            BeginContext(90, 164, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9b712e8a31584d07929001bcacca0a7e", async() => {
                BeginContext(96, 33, true);
                WriteLiteral("\r\n    <title></title>\r\n    \r\n    ");
                EndContext();
                BeginContext(129, 58, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "a038cb8021b6448692f850a1b89d06f7", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(187, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(193, 52, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8b9394d8b5154614a9e4ec6fcbd55730", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(245, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(254, 693, true);
            WriteLiteral(@"
<section class=""content"">
    <div class=""panel panel-default"">
        <div class=""row"">
            <div class=""col-md-8 col-xs-offset-1 MarginLeft "">
                <label class=""radio-inline label ProcessingCustom"">
                    <input type=""radio"" name=""radiobutton"" id=""ProcessingCheckbox"" checked=""checked"">Processing
                </label>
                <label class=""radio-inline label"">
                    <input type=""radio"" name=""radiobutton"" id=""StorageCheckbox"">Storage
                </label>
            </div>
        </div><div style=""margin-top:3%!important""></div>
        <div id=""ProcessingId"">
            <div class=""row"">
                ");
            EndContext();
            BeginContext(948, 121, false);
#line 25 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
           Write(Html.Label("ProcDeviceName", "Device in need of resource", new { @class = "label col-lg-3 col-md-3 col-xs-6 col-sm-6 " }));

#line default
#line hidden
            EndContext();
            BeginContext(1069, 69, true);
            WriteLiteral("\r\n                <div class=\"col-lg-3 col-md-4 col-xs-8 col-sm-6\">\r\n");
            EndContext();
            BeginContext(1270, 20, true);
            WriteLiteral("                    ");
            EndContext();
            BeginContext(1291, 219, false);
#line 28 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
               Write(Html.DropDownListFor(m => m.ProcDeviceName, new SelectList(Model.ProcDeviceName), "-- Select Device --", new { @id = "RAMDeviceId", @class = "form-control", @name = "RAMDevicesLst", @onchange = "GetResourceDetails()" }));

#line default
#line hidden
            EndContext();
            BeginContext(1510, 153, true);
            WriteLiteral("\r\n                </div>\r\n            </div><div style=\"margin-top:3%\"></div>\r\n            <div class=\"row\">\r\n                <div>\r\n                    ");
            EndContext();
            BeginContext(1664, 103, false);
#line 33 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
               Write(Html.Label("AvailableRAM", "Total Availability", new { @class = "label col-md-3 col-xs-6 col-sm-6  " }));

#line default
#line hidden
            EndContext();
            BeginContext(1767, 113, true);
            WriteLiteral("\r\n                </div>\r\n                <div class=\"col-lg-3 col-md-4 col-xs-8 col-sm-6\">\r\n                    ");
            EndContext();
            BeginContext(1881, 106, false);
#line 36 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
               Write(Html.TextBoxFor(m => m.AvailableRAM, new { @id = "AvailableRAMId", @class = "textbox", @readonly = true }));

#line default
#line hidden
            EndContext();
            BeginContext(1987, 297, true);
            WriteLiteral(@"
                </div>
                <div class=""col-xs-1 col-sm-1 pull-left"" style=""padding-left:0;vertical-align: middle;line-height: 2;"">GHz</div>
            </div>
            <div style=""margin-top:3%""></div>
            <div class=""row"">
                <div>
                    ");
            EndContext();
            BeginContext(2285, 110, false);
#line 43 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
               Write(Html.Label("ProcTotalRequirement", "Total Requirement", new { @class = "label col-md-3 col-xs-6 col-sm-6  " }));

#line default
#line hidden
            EndContext();
            BeginContext(2395, 113, true);
            WriteLiteral("\r\n                </div>\r\n                <div class=\"col-lg-3 col-md-4 col-xs-8 col-sm-6\">\r\n                    ");
            EndContext();
            BeginContext(2509, 118, false);
#line 46 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
               Write(Html.TextBoxFor(m => m.ProcTotalRequirement, new { @type = "number", @id = "TotalRequirementId", @class = "textbox" }));

#line default
#line hidden
            EndContext();
            BeginContext(2627, 272, true);
            WriteLiteral(@"
                </div>
                <div class=""col-xs-1 col-sm-1 pull-left"" style=""padding-left:0;vertical-align: middle;line-height: 2;"">GHz</div>
            </div>

            <div style=""margin-top:3%""></div>
            <div class=""row"">
                ");
            EndContext();
            BeginContext(2900, 113, false);
#line 53 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
           Write(Html.Label("ProcDurationNeeded", "Duration Needed", new { @class = "label col-lg-3 col-md-3 col-xs-6 col-sm-6" }));

#line default
#line hidden
            EndContext();
            BeginContext(3013, 89, true);
            WriteLiteral("\r\n                <div class=\"col-lg-3 col-md-4 col-xs-8 col-sm-6\">\r\n                    ");
            EndContext();
            BeginContext(3103, 125, false);
#line 55 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
               Write(Html.TextBoxFor(m => m.ProcDurationNeeded, new { @id = "DurationNeededId", @class = "textbox", @type = "time", @step = "1" }));

#line default
#line hidden
            EndContext();
            BeginContext(3228, 26, true);
            WriteLiteral("\r\n                </div>\r\n");
            EndContext();
            BeginContext(3614, 840, true);
            WriteLiteral(@"            </div>

            <div style=""margin-top:4%""></div>
            <div class=""row"" style=""align-content:center!important;text-align:center!important"">
                <div class=""col-md-3""></div>
                <div class=""col-md-3 col-xs-6 col-sm-6"">
                    <button type=""button"" class=""btn btn-primary btn1"" onclick=""ProcessingReset()"">RESET</button>
                </div>
                <div class=""col-md-3 col-xs-6 col-sm-6"">
                    <button type=""button"" class=""btn btn-primary btn1"" onclick=""UpdateProcessing()"">Start Using</button>
                </div>
            </div><div style=""margin-top:2%""></div>
            <span id=""ValidationMessage"" style=""color:red!important""></span>
        </div>

        <div id=""StorageId"">
            <div class=""row"">
                ");
            EndContext();
            BeginContext(4455, 121, false);
#line 80 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
           Write(Html.Label("StorDeviceName", "Device in need of resource", new { @class = "label col-lg-3 col-md-3 col-xs-6 col-sm-6 " }));

#line default
#line hidden
            EndContext();
            BeginContext(4576, 69, true);
            WriteLiteral("\r\n                <div class=\"col-lg-3 col-md-4 col-xs-8 col-sm-6\">\r\n");
            EndContext();
            BeginContext(4779, 20, true);
            WriteLiteral("                    ");
            EndContext();
            BeginContext(4800, 225, false);
#line 83 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
               Write(Html.DropDownListFor(m => m.StorDeviceName, new SelectList(Model.StorDeviceName), "-- Select Device --", new { @id = "StorageDeviceId", @class = "form-control", @name = "StorageDevicesLst", @onchange = "GetStorageShared()" }));

#line default
#line hidden
            EndContext();
            BeginContext(5025, 153, true);
            WriteLiteral("\r\n                </div>\r\n            </div><div style=\"margin-top:3%\"></div>\r\n            <div class=\"row\">\r\n                <div>\r\n                    ");
            EndContext();
            BeginContext(5179, 107, false);
#line 88 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
               Write(Html.Label("AvailableStorage", "Total Availability", new { @class = "label col-md-3 col-xs-6 col-sm-6  " }));

#line default
#line hidden
            EndContext();
            BeginContext(5286, 113, true);
            WriteLiteral("\r\n                </div>\r\n                <div class=\"col-lg-3 col-md-4 col-xs-8 col-sm-6\">\r\n                    ");
            EndContext();
            BeginContext(5400, 114, false);
#line 91 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
               Write(Html.TextBoxFor(m => m.AvailableStorage, new { @id = "AvailableStorageId", @class = "textbox", @readonly = true }));

#line default
#line hidden
            EndContext();
            BeginContext(5514, 296, true);
            WriteLiteral(@"
                </div>
                <div class=""col-xs-1 col-sm-1 pull-left"" style=""padding-left:0;vertical-align: middle;line-height: 2;"">GB</div>
            </div>
            <div style=""margin-top:3%""></div>
            <div class=""row"">
                <div>
                    ");
            EndContext();
            BeginContext(5811, 109, false);
#line 98 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
               Write(Html.Label("StorTotalRequirement", "Total Requirement", new { @class = "label col-md-3 col-xs-6 col-sm-6 " }));

#line default
#line hidden
            EndContext();
            BeginContext(5920, 113, true);
            WriteLiteral("\r\n                </div>\r\n                <div class=\"col-lg-3 col-md-4 col-xs-8 col-sm-6\">\r\n                    ");
            EndContext();
            BeginContext(6034, 122, false);
#line 101 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
               Write(Html.TextBoxFor(m => m.StorTotalRequirement, new { @type = "number", @id = "TotalRequirementStorId", @class = "textbox" }));

#line default
#line hidden
            EndContext();
            BeginContext(6156, 271, true);
            WriteLiteral(@"
                </div>
                <div class=""col-xs-1 col-sm-1 pull-left"" style=""padding-left:0;vertical-align: middle;line-height: 2;"">GB</div>
            </div>

            <div style=""margin-top:3%""></div>
            <div class=""row"">
                ");
            EndContext();
            BeginContext(6428, 115, false);
#line 108 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
           Write(Html.Label("StorDurationNeeded", "Duration Needed", new { @class = "label col-lg-3 col-md-3 col-xs-6 col-sm-6  " }));

#line default
#line hidden
            EndContext();
            BeginContext(6543, 89, true);
            WriteLiteral("\r\n                <div class=\"col-lg-3 col-md-4 col-xs-8 col-sm-6\">\r\n                    ");
            EndContext();
            BeginContext(6633, 149, false);
#line 110 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\ResourcePool\UseResource.cshtml"
               Write(Html.TextBoxFor(m => m.StorDurationNeeded, new { @id = "StoDurationNeededId", @class = "textbox", @type = "time", @step = "1", @value = "00:00:00" }));

#line default
#line hidden
            EndContext();
            BeginContext(6782, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(7151, 802, true);
            WriteLiteral(@"                </div>
            </div>

            <div style=""margin-top:4%""></div>
            <div class=""row"" style=""align-content:center!important;text-align:center!important"">
                <div class=""col-md-3""></div>
                <div class=""col-md-3 col-xs-6 col-sm-6"">
                    <button type=""button"" class=""btn btn-primary btn1"" onclick=""StorageReset()"">RESET</button>
                </div>
                <div class=""col-md-3 col-xs-6 col-sm-6"">
                    <button type=""button"" class=""btn btn-primary btn1"" onclick=""UpdateStorage()"">START USING</button>
                </div>
            </div>
        </div><div style=""margin-top:2%""></div>
        <span id=""StoValidationMessage"" style=""color:red!important""></span>
    </div>
</section>
");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<DAPP_Model.ERiskDApp.UseResourceModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
