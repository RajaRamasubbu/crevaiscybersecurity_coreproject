#pragma checksum "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\Mydevices\DeviceListPartial.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "50dc7a21d35e94238bbcf35b937064f95037f6ec"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Mydevices_DeviceListPartial), @"mvc.1.0.view", @"/Views/Mydevices/DeviceListPartial.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Mydevices/DeviceListPartial.cshtml", typeof(AspNetCore.Views_Mydevices_DeviceListPartial))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\_ViewImports.cshtml"
using DAPP;

#line default
#line hidden
#line 2 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\_ViewImports.cshtml"
using DAPP.Models;

#line default
#line hidden
#line 4 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#line 1 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\Mydevices\DeviceListPartial.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"50dc7a21d35e94238bbcf35b937064f95037f6ec", @"/Views/Mydevices/DeviceListPartial.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"269350e75962eaa2f635096c18b22f356f66653b", @"/Views/_ViewImports.cshtml")]
    public class Views_Mydevices_DeviceListPartial : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/underscore-min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/devextreme/aspnet/dx.aspnet.mvc.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(35, 46, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "67fb896ad6d344c4bd3e547a3712b1ac", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(81, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(83, 63, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8c357816e8144a2eb2437217f665c8c9", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(146, 6, true);
            WriteLiteral("\r\n\r\n\r\n");
            EndContext();
            BeginContext(154, 2933, false);
#line 6 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\Mydevices\DeviceListPartial.cshtml"
Write(Html.DevExtreme().DataGrid<DAPP_Model.EDMX_MySql.Devicedetails>()
                            .ID("griddevice")
                            .DataSource(d => d
                                .Mvc()
                                .Controller("DevicedetailsGridAPI")
                                .LoadAction("DeviceDetails")
                                .Key("Id")
                                .LoadParams(new { Username = Context.Session.GetString("Username") })
                            )
                            .RemoteOperations(true)
                            .Columns(columns =>
                            {

                                columns.AddFor(m => m.HashId)
                           .Caption("Hash Id");

                                columns.AddFor(m => m.AgentName)
                           .Caption("Agent Name");


                                columns.AddFor(m => m.Macaddress)
                           .Caption("MAC ID");

                                columns.AddFor(m => m.DeviceCreatedAt)
                           .Caption("Time Stamp");

                                columns.AddFor(m => m.Biosid)
                           .Caption("BIOS ID");

                                columns.Add().DataField("Id").Caption("")
                           .CellTemplate(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    BeginContext(1516, 3, true);
    WriteLiteral(" <a");
    EndContext();
    BeginWriteAttribute("href", " href=\"", 1519, "\"", 1559, 3);
#line 36 "D:\Projects\DApp\Source MVCCore\DAPP\DAPP\Views\Mydevices\DeviceListPartial.cshtml"
WriteAttributeValue("", 1526, Url.Action("Edit"), 1526, 19, false);

#line default
#line hidden
    WriteAttributeValue("", 1545, "/<%=", 1545, 4, true);
    WriteAttributeValue(" ", 1549, "data.Id%>", 1550, 10, true);
    EndWriteAttribute();
    BeginContext(1560, 71, true);
    WriteLiteral(" onclick=\"ViewLogDetails(\'<%= data.Id%>\'); return false;\">View Log</a> ");
    EndContext();
    PopWriter();
}
))
                                                       .Alignment(HorizontalAlignment.Center);

                                                        })
                                                        .FilterRow(f => f.Visible(true))
                                                        .HeaderFilter(f => f.Visible(true))
                                                        .GroupPanel(p => p.Visible(true))
                                                        //.Scrolling(s => s.Mode(GridScrollingMode.Virtual))
                                                        //.Height(500)
                                                        .ShowBorders(true)
                                                        .Grouping(g => g.AutoExpandAll(false))
                                                        .Paging(paging => paging.PageSize(10))
                                                        .Pager(pager =>
                                                        {
                                                            pager.ShowPageSizeSelector(true);
                                                            pager.AllowedPageSizes(new List<int> { 5, 10, 20 });
                                                            pager.ShowInfo(true);
                                                        })
                                                        .FilterPanel(f => f.Visible(true))

);

#line default
#line hidden
            EndContext();
            BeginContext(3102, 8, true);
            WriteLiteral("\r\n\r\n\r\n\r\n");
            EndContext();
            BeginContext(8938, 2, true);
            WriteLiteral("\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
