﻿var urlAllias = window.location.href.split('/')[3];
$(document).ready(function () {
    document.getElementById("PageHeader").innerHTML = "Setting";
    $('.active').removeClass('active');
    $('#SettingsId').addClass('active');
});

function DisbleDeviceLst() {
    document.getElementById("DevicesLst").disabled = true;
}
function EnableDeviceLst() {
    document.getElementById("DevicesLst").disabled = false;
}

function Reload() {
    location.reload();
}

function UpdateShareResourceSetting() {

    debugger;
    var selectedVal = "";
    var TurnOffFor = $("#RadioId input[type='radio']:checked");

    if (TurnOffFor.length > 0) {
        selectedVal = TurnOffFor.val();
    }

    //var selectedVal = $('input[name=TurnOffFor]:checked').val();
    var _DeviceId = $("#DevicesLst").val();
    var DeactiveFrom = $("#DeactiveFromId").val();
    var DeactiveTo = $("#DeactiveToId").val();
    var ValidationVal = ValideSettingsUpdate(selectedVal, _DeviceId, DeactiveFrom, DeactiveTo);
    if (ValidationVal == true) {

        if (selectedVal == "2") {
            bootbox.confirm("Are you sure, you want to stop the resource sharing?",
                function (result) {
                    if (result == true) {
                        $('#LoadingPanel').addClass('dvLoading');
                        var URL = "";
                        if (urlAllias == 'DAppSetting') {
                            URL = '/ResourcePool/StopResourceSharing';
                        }
                        else {
                            URL = '/' + urlAllias + '/ResourcePool/StopResourceSharing';
                        }
                        $.ajax({
                            url: URL,
                            type: "POST",
                            data: { ShareResourceId: _DeviceId },
                            dataType: "text",
                            success: function (response) {
                                $('#LoadingPanel').removeClass('dvLoading');
                                if (response == "true") {
                                    bootbox.alert("Resource Sharing Stopped Successfully.", function () { location.reload(true); });
                                }
                                else {
                                    bootbox.alert("Failed to stop resource sharing, Please try again.", function () { location.reload(true); });
                                }
                            }

                        });
                    }
                }
            );
        }
        else {
            var URL = "";
            if (urlAllias == 'DAppSetting') {
                URL = '/DAppSetting/UpdateResSharingSetting';
            }
            else {
                URL = '/' + urlAllias + '/DAppSetting/UpdateResSharingSetting';
            }
            //debugger;
            $.ajax({
                url: URL,
                type: "POST",
                data: { TurnOffFor: parseInt(selectedVal), DeviceName: _DeviceId.toString(), TurnOffFrom: DeactiveFrom, TurnOffTo: DeactiveTo },
                dataType: "text",
                success: function (response, status, xhr) {
                    if (response == "True") {
                        location.reload();
                    }
                }, error: function () {
                }
            });
        }
    }
}

function ValideSettingsUpdate(selectedVal, DeviceId, DeactiveFrom, DeactiveTo) {
    debugger;
    if (selectedVal == "" && DeactiveFrom == (null || "") && DeactiveTo == (null || "")) {
        document.getElementById("SettingsValidationMessage").innerHTML = "Please select anyone setting option to update.";
    }
    else {

        if (selectedVal != "") {
            //document.getElementById("SettingsValidationMessage").innerHTML = "Select atleast one value for 'Turn off Resource Sharing' field";
            //return false;
            if (selectedVal == "2" && DeviceId == "") {
                document.getElementById("SettingsValidationMessage").innerHTML = "Please select Device Name from dropdown";
                return false;
            }
        }
        else {
            if (DeactiveFrom == (null || "")) {
                document.getElementById("SettingsValidationMessage").innerHTML = "Please mention Deactive Resource Sharing From Time";
                return false;
            } else if (DeactiveTo == (null || "")) {
                document.getElementById("SettingsValidationMessage").innerHTML = "Please mention Deactive Resource Sharing To Time";
                return false;
            }
        }
        document.getElementById("SettingsValidationMessage").innerHTML = "";
        return true;
    }
}