﻿var urlAllias = window.location.href.split('/')[3];
//var Baseurl = window.location.href;
$(document).ready(function () {
    //debugger;
    var LogsDetails = document.getElementById('LogsListId');
    var Gridheader = document.getElementById('GridHeader');

    $('#LoadingPanel').addClass('dvLoading');
    document.getElementById("PageHeader").innerHTML = "My Devices";
    $('.active').removeClass('active');
    $('#MyDeviceId').addClass('active');
    LogsDetails.style.display = "none";
    Gridheader.style.display = "none";
    //debugger;
    var URL = "";
    if (urlAllias == 'Mydevices') {
        URL = '/Mydevices/DeviceListPartial';
    }
    else {
        URL = '/' + urlAllias + '/Mydevices/DeviceListPartial';
    }
    $.ajax({
        url: URL,
        type: "GET",
        data: {},
        dataType: "text",
        success: function (response, status, xhr) {
            //debugger
            var ct = xhr.getResponseHeader("content-type") || "";
            if (ct.indexOf('html') > -1) {
                $("#DevicesGrid").html(response);
                $('#LoadingPanel').removeClass('dvLoading');
                Gridheader.style.display = "block";
            }
        }, error: function () {
            alert("Error while rendering grid");
        }
    });
});

setInterval(function () {
    var URL = "";
    if (urlAllias == 'Mydevices') {
        URL = '/Mydevices/GetDevicesOnBlockChainCount';
    }
    else {
        URL = '/' + urlAllias + '/Mydevices/GetDevicesOnBlockChainCount';
    }
    $.ajax({
        url: URL,
        type: "GET",
        data: {},
        contentType: "application/json",
        success: function (response, status, xhr) {
            document.getElementById("OnBlockChainId").innerHTML = response.OnBlockChain;
            document.getElementById("InProgessId").innerHTML = response.InProgress;
        }, error: function () {
            //alert("false");
        }
    });
}, 1000 * 60 * 1);

function ViewLogDetails(Id) {
    //debugger
    var LogsDetails = document.getElementById('LogsListId');
    var URL = "";
    if (urlAllias == 'Mydevices') {
        URL = '/Mydevices/GetLogDetails';
    }
    else {
        URL = '/' + urlAllias + '/Mydevices/GetLogDetails';
    }
    $.ajax({
        url: URL,
        type: "GET",
        data: { DeviceId: Id },
        contentType: "application/json",
        success: function (response, status, xhr) {
            //debugger;
            var Logs = "";
            if (response.length > 0 && response!="[]") {
                var data = JSON.stringify(response);
                //var Logdata = data.split(',').join('\n\n');         
                for (var i = 0; i < data.length; i++) {
                    Logs += data[i];
                }
                Logs = Logs.replace(/,/g, "<br><br>").slice(1, -1).replace(/["]+/g, '');
            }
            else {
                Logs = "No Logs to display";
            }
            LogsDetails.style.display = "block";
            document.getElementById("LogsList").innerHTML = Logs;
        }, error: function () {
            alert("Error while displaying logs");
        }
    });
}

function CloseLogs() {
    var LogsDetails = document.getElementById('LogsListId');
    LogsDetails.style.display = "none";
}

