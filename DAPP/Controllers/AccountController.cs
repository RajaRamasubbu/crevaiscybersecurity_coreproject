﻿using System;
using DAPP_Logger;
using DAPP_Model.Accounts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DAPP.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public AccountController()
        {
        }


        [AllowAnonymous]
        public ActionResult DAppLogin()
        {
            //try
            //{

            //    Renci.SshNet.SshCommand cmd;
            //    Renci.SshNet.PasswordConnectionInfo connInfo = new Renci.SshNet.PasswordConnectionInfo("192.168.3.47","balaji","winjit123");
            //    Renci.SshNet.SshClient sshClient = new Renci.SshNet.SshClient(connInfo);

            //    sshClient.Connect();

            //    cmd = sshClient.RunCommand("sh /home/balaji/test.sh");
            //    string answer = cmd.Result;

            //    sshClient.Disconnect();
            //}
            //catch (Exception ex)
            //{
            //   throw ex;
            //}

            //using (InvokeShellScripts shellscripts = new InvokeShellScripts())
            //{
            //    bool result = shellscripts.RunShellScriptCommand();
            //}
            HttpContext.Session.SetString("Username", "");
            HttpContext.Session.SetString("User", "");
            HttpContext.Session.SetString("Token", "");
            HttpContext.Session.SetString("ShareResourceGrid", "");
            DAppLoginModel model = new DAppLoginModel();
            string CustomerId = Request.Cookies["CustomerId"];
            string Password = Request.Cookies["Password"];
            string URL = Request.Cookies["URL"];
            if (CustomerId != null && Password != null && URL != null)
            {
                model.CustomerId = CustomerId;
                model.Password = Password;
                model.URL = URL;
                return View(model);
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public void SetCookies(string CustomerId, string Password, string URL)
        {
            try
            {
                //HttpCookie cookie = new HttpCookie("DAppLoginInfo");
                //cookie["CustomerID"] = CustomerId;
                ////cookie["Password"] = EncryptionAndDecryption.DecryptStringAES(model.Password);
                //cookie["Password"] = Password;
                //cookie.Expires = DateTime.Now.AddDays(30);
                //Response.Cookies.Add(cookie);
                //Response.Redirect("LoginWeb","Account");

                CookieOptions option = new CookieOptions();
                option.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Append("CustomerId", CustomerId, option);
                Response.Cookies.Append("Password", Password, option);
                Response.Cookies.Append("URL", URL, option);

            }
            catch (Exception e)
            {
                Logger.Error(e, "SetCookies Method");
                throw e;
            }
        }


        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        public void UpdateUserNameSession(string Username, string Token)
        {
            HttpContext.Session.SetString("Username", Username);
            HttpContext.Session.SetString("Token", Token);
        }
    }
}