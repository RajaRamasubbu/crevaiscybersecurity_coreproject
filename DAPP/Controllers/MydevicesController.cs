﻿using DAPP.ERiskCommon;
using DAPP_Logger;
using DAPP_Model.ERiskDApp;
using DAPP_Service.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Renci.SshNet;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DAPP.Controllers
{
    [SessionExpire]
    public class MydevicesController : Controller
    {
        //private readonly DAPPContext _context;
        public ActionResult DeviceDetails()
        {
            try
            {

                string Username = HttpContext.Session.GetString("Username").ToString();
                MyDevices DevicesModel = new MyDevices();
                DevicesModel.TotalDevices = CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == Username).Count();
                DevicesModel.OnBlockChain = CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == Username && x.HashId != null).Count();
                DevicesModel.InProgress = CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == Username && x.HashId == null).Count();

                using (MyDevicesService deviceservice = new MyDevicesService())
                {
                    deviceservice.GenerateHashIdForDevices(Username);
                }

                return View(DevicesModel);
            }

            catch (Exception e)
            {
                Logger.Error(e, "DeviceDetails Method");
                throw e;
            }
        }

        public ActionResult DeviceListPartial()
        {
            string Username = HttpContext.Session.GetString("Username").ToString();
            //List<Devicedetails> model = CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == Username).ToList();
            //, model
            return PartialView("DeviceListPartial");
        }

        public bool UpdateDeviceDetails(string Username, string Users)
        {
            UserList _Users = new UserList();
            _Users = JsonConvert.DeserializeObject<UserList>(Users);

            using (MyDevicesService service = new MyDevicesService())
            {
                HttpContext.Session.SetString("User", _Users.name);
                bool UpdateDeviceList = service.UpdateDeviceDetails(Username, _Users);
                return UpdateDeviceList;
            }

        }

        public JsonResult GetDevicesOnBlockChainCount()
        {
            try
            {
                string UserName = (HttpContext.Session.GetString("Username") == null || HttpContext.Session.GetString("Username") == "") ? null : HttpContext.Session.GetString("Username").ToString();
                var DevicesDetails = new
                {
                    OnBlockChain = CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == UserName && x.HashId != null).Count(),
                    InProgress = CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == UserName && x.HashId == null).Count()
                };
                return Json(DevicesDetails); //, JsonRequestBehavior.AllowGet
            }
            catch (Exception e)
            {
                DAPP_Logger.Logger.Error(e, "GetDevicesOnBlockChainCount Method");
                throw e;
            }

        }

        [HttpGet]
        public JsonResult GetLogDetails(long DeviceId)
        {
            try
            {
                using (MyDevicesService service = new MyDevicesService())
                {
                    var LogsList = service.GetLogList(DeviceId).ToList();
                    return Json(LogsList);
                }
            }
            catch (Exception e)
            {
                DAPP_Logger.Logger.Error(e, "GetLogDetails Method");
                throw e;
            }
        }
        public void Logs()
        {
        }

    }
}