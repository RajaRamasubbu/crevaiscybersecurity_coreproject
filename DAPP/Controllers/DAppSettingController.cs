﻿using System;
using DAPP_Logger;
using System.Collections.Generic;
using System.Linq;
using DAPP_Model.ERiskDApp;
using DAPP_Service.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using DAPP_Service.Service;

namespace DAPP.Controllers
{
    //[SessionExpire]
    public class DAppSettingController : Controller
    {
        // GET: DAppSetting
        public ActionResult ResourceShareSetting()
        {
            try
            {
                ResourceShareSettingsModel model = new ResourceShareSettingsModel();
                string Username = HttpContext.Session.GetString("Username").ToString();
                List<DropdownList> DevicenameLst = CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == Username).Select(x => new DropdownList { Id = x.Id, Text = x.DeviceName }).ToList();
                //List<string> DevicenameLst = CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == Username).Select(x => x.DeviceName).ToList();
                model.DeviceNamelst = DevicenameLst;
                return View(model);
            }
            catch (Exception e)
            {
               Logger.Error(e, "ResourceShareSetting Method");
                throw e;
            }
        }

        public bool UpdateResSharingSetting(int TurnOffFor, string DeviceName, TimeSpan TurnOffFrom, TimeSpan TurnOffTo)
        {
            try
            {
                using (ShareResourceSettingService service = new ShareResourceSettingService())
                {
                    string Username = HttpContext.Session.GetString("Username").ToString();
                    bool returnVal = service.ShareProcessingSettingsUpdate(TurnOffFor, DeviceName, TurnOffFrom, TurnOffTo, Username);
                    return true;
                }
            }
            catch (Exception e)
            {
               Logger.Error(e, "UpdateResSharingSetting");
                throw e;
            }
        }
    }
}