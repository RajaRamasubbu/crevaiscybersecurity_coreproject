﻿using DAPP.ERiskCommon;
using DAPP_Logger;
using DAPP_Model.Common;
using DAPP_Model.ERiskDApp;
using DAPP_Service.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAPP.Controllers
{
    [SessionExpire]
    public class ResourcePoolController : Controller
    {
        // GET: ResourcePool
        public ActionResult Dashboard(string DeviceName)
        {
            try
            {
                DashboardModel model = new DashboardModel();
                string Username = HttpContext.Session.GetString("Username").ToString();
                List<DropdownList> DevicenameLst = CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == Username).Select(x => new DropdownList { Id = x.Id, Text = x.DeviceName }).ToList();
                model.DeviceNamelst = DevicenameLst;
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Dashboard Method");
                throw e;
            }
        }

        public ActionResult DashBoardPartial(long DeviceId)
        {
            try
            {
                using (ResourcePoolService service = new ResourcePoolService())
                {
                    DashboardModel model = new DashboardModel();
                    string Username = HttpContext.Session.GetString("Username").ToString();
                    if (DeviceId != 0)
                    {
                        model.RAMAvailabity = decimal.Round(Convert.ToDecimal(CommonClass.DB.Devicedetails.Where(x => x.Id == DeviceId && x.CreatedBy == Username).Select(x => x.Processing).FirstOrDefault()) / 1024, 3, MidpointRounding.AwayFromZero);
                        model.StoAvailabity = decimal.Round(Convert.ToDecimal(CommonClass.DB.Devicedetails.Where(x => x.Id == DeviceId && x.CreatedBy == Username).Select(x => x.Storage).FirstOrDefault()) / 1024, 3, MidpointRounding.AwayFromZero);
                        model.RAMSurplus = service.GetAvailableRAM(DeviceId);
                        model.StoSurplus = service.GetAvailableStorage(DeviceId);
                        model.RAMUtilization = service.GetRAMUtilizedResource(DeviceId) == null ? 0 : service.GetRAMUtilizedResource(DeviceId);
                        model.StoUtilization = service.GetStorageUtilizedResource(DeviceId) == null ? 0 : service.GetStorageUtilizedResource(DeviceId);
                    }
                    else
                    {
                        model.RAMAvailabity = 0;
                        model.StoAvailabity = 0;
                        model.RAMSurplus = 0;
                        model.StoSurplus = 0;
                        model.RAMUtilization = 0;
                        model.StoUtilization = 0;
                    }
                    return PartialView("DashBoardPartial", model);
                }

            }
            catch (Exception e)
            {
                Logger.Error(e, "DashBoardPartial Method");
                throw e;
            }
        }

        public ActionResult ShareResource()
        {
            try
            {
                ResourcePoolModel model = new ResourcePoolModel();
                string Username = HttpContext.Session.GetString("Username").ToString();
                List<DropdownList> DevicenameLst = CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == Username).Select(x => new DropdownList { Id = x.Id, Text = x.DeviceName }).ToList();
                model.DeviceNamelst = DevicenameLst;
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Error(e, "ShareResource");
                throw e;
            }
        }

        public decimal? GetTotalResourceAvailability(string ResourceType, long DeviceId)
        {

            try
            {
                using (ResourcePoolService Service = new ResourcePoolService())
                {
                    decimal? TotalAvailability = 0;
                    if (ResourceType == "RAM")
                    {
                        TotalAvailability = Service.GetAvailableResource(DeviceId, ResourceType);
                    }
                    else if (ResourceType == "Storage")
                    {
                        TotalAvailability = Service.GetAvailableResource(DeviceId, ResourceType);
                    }
                    return TotalAvailability;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetTotalResourceAvailability Method");
                throw e;
            }
        }

        public bool ShareResourceProcessing(long DeviceId, decimal TotalAvailabity, decimal SharedResource, string ResourceType, long ShareToDeviceId)
        {
            try
            {
                using (ResourcePoolService Service = new ResourcePoolService())
                {

                    string Username = HttpContext.Session.GetString("Username").ToString();
                    if (ResourceType == "RAM")
                    {
                        bool returnval = Service.ShareProcessingUpdate(DeviceId, TotalAvailabity, SharedResource, Username, ShareToDeviceId);

                        return returnval;
                    }
                    else if (ResourceType == "Storage")
                    {
                        bool returnval = Service.ShareStorageUpdate(DeviceId, TotalAvailabity, SharedResource, Username, ShareToDeviceId);
                        return returnval;
                    }
                    return false;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ShareResourceProcessing Method");
                return false;
            }
        }

        //public void UpdateShareResourceSession(long DeviceId, decimal TotalAvailabity, decimal SharedResource, string ResourceType)
        //{
        //    try
        //    {

        //        ShareResourceSession model = new ShareResourceSession();
        //        if (HttpContext.Session.GetString("ShareResourceGrid") == null || HttpContext.Session.GetString("ShareResourceGrid") == "")
        //            HttpContext.Session.SetObjectAsJson("ShareResourceGrid", new List<ShareResourceSession>());

        //        //model.DeviceName = DeviceName;
        //        model.TotalAvailability = TotalAvailabity;
        //        model.ResourceType = ResourceType == "RAM" ? 1 : 2;
        //        model.ShareResource = SharedResource;
        //        var ShareResourcelst = HttpContext.Session.GetObjectFromJson<List<ShareResourceSession>>("ShareResourceGrid");
        //        ShareResourcelst.Add(model);
        //        HttpContext.Session.SetObjectAsJson("ShareResourceGrid", ShareResourcelst);
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.Error(e, "UpdateShareResourceSession Method");
        //        throw e;
        //    }
        //}

        //public ActionResult ShareResourceGrid()
        //{
        //    try
        //    {
        //        List<ShareResourceGrid> model = new List<ShareResourceGrid>();
        //        ShareResourceGrid modelVal = new ShareResourceGrid();

        //        if (HttpContext.Session.GetString("ShareResourceGrid") == null|| HttpContext.Session.GetString("ShareResourceGrid") == "")
        //        {
        //            var RAMSessionVal = HttpContext.Session.GetObjectFromJson<List<ShareResourceSession>>("ShareResourceGrid"); 
        //            foreach (var item in RAMSessionVal)
        //            {
        //                modelVal = new ShareResourceGrid();
        //                modelVal.DeviceName = item.DeviceName;
        //                //modelVal.ResourceType = item.ResourceType;
        //                if (item.ResourceType == 1)
        //                {
        //                    modelVal.ShareResource = ((decimal)(item.ShareResource)).ToString("N2") + "GHz";
        //                    modelVal.TotalAvailability = ((decimal)(item.TotalAvailability)).ToString("N2") + "GHz";
        //                }
        //                else
        //                {
        //                    modelVal.ShareResource = ((decimal)(item.ShareResource)).ToString("N2") + "GHz";
        //                    modelVal.TotalAvailability = ((decimal)(item.TotalAvailability)).ToString("N2") + "GB";
        //                }

        //                model.Add(modelVal);
        //            }
        //        }
        //        else
        //        {
        //            modelVal = new ShareResourceGrid();
        //            modelVal.DeviceName = null;
        //            modelVal.ResourceType = null;
        //            modelVal.ShareResource = null;
        //            modelVal.TotalAvailability = null;
        //            model.Add(modelVal);
        //        }
        //        return PartialView("ShareResourceGrid", model);
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.Error(e, "ShareResourceGrid Method");
        //        throw e;
        //    }
        //}

        //public ActionResult ShareResourceGridJson()
        //{
        //    try
        //    {
        //        List<ShareResourceGrid> model = new List<ShareResourceGrid>();
        //        ShareResourceGrid modelVal = new ShareResourceGrid();

        //        if (HttpContext.Session.GetString("ShareResourceGrid") != null && HttpContext.Session.GetString("ShareResourceGrid") != "")
        //        {
        //            var RAMSessionVal = HttpContext.Session.GetObjectFromJson<List<ShareResourceSession>>("ShareResourceGrid");
        //            foreach (var item in RAMSessionVal)
        //            {
        //                modelVal = new ShareResourceGrid();
        //                modelVal.DeviceName = item.DeviceName;
        //                //below line commented by raja as i changed the data type to string from int
        //               // modelVal.ResourceType = item.ResourceType;
        //                if (item.ResourceType == 1)
        //                {
        //                    modelVal.ShareResource = ((decimal)(item.ShareResource)).ToString("N2") + "GHz";
        //                    modelVal.TotalAvailability = ((decimal)(item.TotalAvailability)).ToString("N2") + "GHz";
        //                }
        //                else
        //                {
        //                    modelVal.ShareResource = ((decimal)(item.ShareResource)).ToString("N2") + "GHz";
        //                    modelVal.TotalAvailability = ((decimal)(item.TotalAvailability)).ToString("N2") + "GB";
        //                }

        //                model.Add(modelVal);
        //            }
        //        }
        //        else
        //        {
        //            modelVal = new ShareResourceGrid();
        //            modelVal.DeviceName = null;
        //            modelVal.ResourceType = null;
        //            modelVal.ShareResource = null;
        //            modelVal.TotalAvailability = null;
        //            model.Add(modelVal);
        //        }
        //        return Json(model);
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.Error(e, "ShareResourceGridJson Method");
        //        throw e;
        //    }
        //}


        //----------------------------------UseResource----------------------------------------------------------------
        public ActionResult UseResource()
        {
            try
            {
                using (ResourcePoolService service = new ResourcePoolService())
                {
                    UseResourceModel model = new UseResourceModel();

                    if (HttpContext.Session.GetString("ShareResourceGrid") == null || HttpContext.Session.GetString("ShareResourceGrid") == "")
                    {
                        model.ProcDeviceName = new List<string>();
                        model.StorDeviceName = new List<string>();
                    }
                    else
                    {
                        if ((HttpContext.Session.GetObjectFromJson<List<ShareResourceSession>>("ShareResourceGrid")).Where(x => x.ResourceType == 1).Count() == 0)
                            model.ProcDeviceName = new List<string>();
                        else
                            model.ProcDeviceName = (HttpContext.Session.GetObjectFromJson<List<ShareResourceSession>>("ShareResourceGrid")).Where(x => x.ResourceType == 1).Select(x => x.DeviceName).ToList();

                        if ((HttpContext.Session.GetObjectFromJson<List<ShareResourceSession>>("ShareResourceGrid")).Where(x => x.ResourceType == 2).Count() == 0)
                            model.StorDeviceName = new List<string>();
                        else
                            model.StorDeviceName = (HttpContext.Session.GetObjectFromJson<List<ShareResourceSession>>("ShareResourceGrid")).Where(x => x.ResourceType == 2).Select(x => x.DeviceName).ToList();
                    }
                    return View(model);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "UseResource Method");
                throw e;
            }
        }

        public decimal? GetRAMShared(string DeviceName)
        {
            try
            {
                using (ResourcePoolService service = new ResourcePoolService())
                {
                    //model.AvailableRAM = service.GetAvailableRAM(Session["ProcDeviceName"].ToString());
                    //return service.GetAvailableRAM(DeviceName);
                    long deviceId = 0;
                    return service.GetAvailableRAM(deviceId);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetRAMShared Method");
                throw e;
            }
        }

        public decimal? GetStorageShared(string DeviceName)
        {
            try
            {
                using (ResourcePoolService service = new ResourcePoolService())
                {
                    //model.AvailableRAM = service.GetAvailableRAM(Session["ProcDeviceName"].ToString());
                    long deviceId = 0;
                    return service.GetAvailableStorage(deviceId);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetStorageShared Method");
                throw e;
            }
        }

        public bool UpdateUseResourceProcessing(string DeviceName, decimal RAMRequired, string RequiredTime)
        {
            try
            {
                using (ResourcePoolService Service = new ResourcePoolService())
                {
                    TimeSpan _RequiredTime = TimeSpan.Parse(RequiredTime);
                    string Username = HttpContext.Session.GetString("Username").ToString();
                    bool returnval = Service.UseResProcessingUpdate(DeviceName, RAMRequired, _RequiredTime, Username);

                    using (InvokeShellScripts shellscripts = new InvokeShellScripts())
                    {
                        bool result = shellscripts.RunShellScriptCommand(true, "");
                    }

                    return returnval;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "UpdateUseResourceProcessing Method");
                throw e;
            }
        }

        public bool UpdateUseResourceStorage(string DeviceName, decimal StorageRequired, TimeSpan RequiredTime)
        {
            try
            {
                using (ResourcePoolService Service = new ResourcePoolService())
                {
                    string Username = HttpContext.Session.GetString("Username").ToString();
                    bool returnval = Service.UseResStorageUpdate(DeviceName, StorageRequired, RequiredTime, Username);

                    using (InvokeShellScripts shellscripts = new InvokeShellScripts())
                    {
                        bool result = shellscripts.RunShellScriptCommand(true, "");
                    }
                    return returnval;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "UpdateUseResourceStorage Method");
                throw e;
            }
        }

        public bool StopResourceSharing(long ShareResourceId)
        {
            try
            {
                using (ResourcePoolService Service = new ResourcePoolService())
                {
                    string Username = HttpContext.Session.GetString("Username").ToString();
                    bool returnval = Service.StopResourceSharing(ShareResourceId, Username);

                    return returnval;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "StopResourceSharing Method");
                //throw e;
                return false;
            }
        }
    }
}

