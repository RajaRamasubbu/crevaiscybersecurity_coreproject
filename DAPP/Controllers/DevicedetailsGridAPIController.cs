﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAPP_Logger;
using DAPP_Model.EDMX_MySql;
using DAPP_Model.ERiskDApp;
using DAPP_Service.Service;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DAPP.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DevicedetailsGridAPIController : ControllerBase
    {
        // GET: api/DevicedetailsGridAPI
        [HttpGet]
        public object DeviceDetails(DataSourceLoadOptions loadOptions, string Username)
        {
            using (dappContext context = new dappContext())
            {
                return DataSourceLoader.Load(context.Devicedetails.Where(x => x.CreatedBy == Username).ToList(), loadOptions);
            }
        }


        [HttpGet]
        public object SharedResourceList(DataSourceLoadOptions loadOptions, string Username)
        {
            using (dappContext context = new dappContext())
            {

                List<ShareResourceGrid> details = new List<ShareResourceGrid>();

                details = context.Shareresourcedetails
                                .Include(s => s.Device)
                                .Include(s => s.SharedToDevice)
                                .Include(s => s.ResourceTypeNavigation).ToList().Where(x => x.CreatedBy == Username).ToList()
                                .Select(y => new ShareResourceGrid
                                {
                                    Id = y.Id,
                                    DeviceName = y.Device.DeviceName,
                                    ResourceType = y.ResourceTypeNavigation.ResourceType1,
                                    // ShareResource = (y.ResourceType == 1) ? (Convert.ToDecimal(y.ProcessingShared)).ToString("N2") + " GHz" : (Convert.ToDecimal(y.StorageShared)/1024).ToString("N2") + " GB",
                                    //TotalAvailability = (y.ResourceType == 1) ? (Convert.ToDecimal(y.TotalAvailability)).ToString("N2") + " GHz" : (Convert.ToDecimal(y.TotalAvailability) / 1024).ToString("N2") + " GB",
                                    ShareResource = (y.ResourceType == 1) ? (Convert.ToDecimal(y.ProcessingShared) / 1024).ToString("N3") + " GB" : (Convert.ToDecimal(y.StorageShared) / 1024).ToString("N3") + " GB",
                                    TotalAvailability = (y.ResourceType == 1) ? (Convert.ToDecimal(y.TotalAvailability) / 1024).ToString("N3") + " GB" : (Convert.ToDecimal(y.TotalAvailability) / 1024).ToString("N3") + " GB",
                                    ShareStatus = Convert.ToBoolean(y.IsShareStopped),
                                    SharedToDeviceName = (y.SharedToDevice != null) ? y.SharedToDevice.DeviceName : null

                                }).ToList();

                return DataSourceLoader.Load(details.ToList().OrderByDescending(y => y.Id), loadOptions);
            }
        }

        [Route("PostData")]
        [HttpPost]
        public async Task<ContentResult> PostData(SmartContractAPIModel model)
        {
            //ResponseMessage reponsemessage = new ResponseMessage();
            try
            {
                string HashId = "";
                using (MyDevicesService mydevice = new MyDevicesService())
                {
                    var DeviceDetails = mydevice.DeviceAlreadyExist(model.UserName, model.MacAddress);
                    if (DeviceDetails == null)
                    {
                        HashId = await mydevice.GetHashIdForNewDevice(model);
                    }
                    else
                        HashId = DeviceDetails.HashId;

                    //reponsemessage.Status = HttpStatusCode.OK;
                    //reponsemessage.HashId = HashId;

                    return new ContentResult
                    {
                        Content = HashId,//JsonConvert.SerializeObject(reponsemessage),
                        ContentType = "text/plain",
                        StatusCode = 200
                    };
                }
            }

            catch (Exception e)
            {
                Logger.Error(e, "PostData API");

                //reponsemessage.Status = HttpStatusCode.ExpectationFailed;
                return new ContentResult
                {
                    Content = null,
                    ContentType = "text/plain",
                    StatusCode = 417
                };
            }
        }


    }
}
