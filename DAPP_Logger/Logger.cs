﻿using DAPP_Model.EDMX_MySql;
using Microsoft.EntityFrameworkCore.Storage;
using System;

namespace DAPP_Logger
{
    public static class Logger
    {
        public static void Error(Exception ex, string MessageTemplate)
        {
            try
            {
                using (var context = new dappContext())
                {
                    using (IDbContextTransaction Tran = context.Database.BeginTransaction())
                    {
                        try
                        {
                            Errorlog log = new Errorlog();

                            log.Logger = MessageTemplate;
                            log.Date = DateTime.Now;
                            log.Level = "Error";
                            log.Message = Convert.ToString(ex.Message);
                            log.Exception = Convert.ToString(ex.InnerException);
                            log.Thread = Convert.ToString(ex.StackTrace);
                            context.Errorlog.Add(log);

                            context.SaveChanges();
                            Tran.Commit();
                        }
                        catch (Exception e)
                        {
                            Tran.Rollback();
                            Tran.Dispose();
                        }
                    }
                }


            }
            catch (Exception e)
            {

                throw e;
            }
        }

        //public void InformationLog(object message)
        //{
        //    logger.Info(message);
        //}

        //public void FatalLog(object message)
        //{
        //    logger.Fatal(message);
        //}

        //public void DebugLog(object message)
        //{
        //    logger.Debug(message);
        //}

        //public void WarnLog(object message)
        //{
        //    logger.Warn(message);
        //}

    }
}
