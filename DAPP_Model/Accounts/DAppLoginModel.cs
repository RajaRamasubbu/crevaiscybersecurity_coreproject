﻿namespace DAPP_Model.Accounts
{
    public class DAppLoginModel
    {
        public string CustomerId { get; set; }
        public string Password { get; set; }
        public string IsRemember { get; set; }
        public string URL { get; set; }
    }
}
