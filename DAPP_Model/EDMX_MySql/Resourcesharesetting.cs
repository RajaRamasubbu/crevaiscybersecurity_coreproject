﻿using System;
using System.Collections.Generic;

namespace DAPP_Model.EDMX_MySql
{
    public partial class Resourcesharesetting
    {
        public long Id { get; set; }
        public int? TurnOffFor { get; set; }
        public long? DeviceId { get; set; }
        public TimeSpan? TurnOffFrom { get; set; }
        public TimeSpan? TurnOffTo { get; set; }
        public sbyte? IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }

        public Devicedetails Device { get; set; }
    }
}
