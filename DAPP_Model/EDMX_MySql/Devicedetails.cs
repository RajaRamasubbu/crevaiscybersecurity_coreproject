﻿using System;
using System.Collections.Generic;

namespace DAPP_Model.EDMX_MySql
{
    public partial class Devicedetails
    {
        public Devicedetails()
        {
            Changelogheader = new HashSet<Changelogheader>();
            Resourcesharesetting = new HashSet<Resourcesharesetting>();
            ShareresourcedetailsDevice = new HashSet<Shareresourcedetails>();
            ShareresourcedetailsSharedToDevice = new HashSet<Shareresourcedetails>();
            Useresourcedetails = new HashSet<Useresourcedetails>();
        }

        public long Id { get; set; }
        public string HashId { get; set; }
        public long? AgentId { get; set; }
        public string AgentName { get; set; }
        public string DeviceName { get; set; }
        public string Macaddress { get; set; }
        public string Biosid { get; set; }
        public int? DeviceStatus { get; set; }
        public DateTime? DeviceCreatedAt { get; set; }
        public DateTime? DeviceUpdatedAt { get; set; }
        public decimal? Storage { get; set; }
        public decimal? StorageMax { get; set; }
        public decimal? Processing { get; set; }
        public decimal? ProcessingMax { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifiedBy { get; set; }
        public long? EriskDeviceId { get; set; }

        public ICollection<Changelogheader> Changelogheader { get; set; }
        public ICollection<Resourcesharesetting> Resourcesharesetting { get; set; }
        public ICollection<Shareresourcedetails> ShareresourcedetailsDevice { get; set; }
        public ICollection<Shareresourcedetails> ShareresourcedetailsSharedToDevice { get; set; }
        public ICollection<Useresourcedetails> Useresourcedetails { get; set; }
    }
}
