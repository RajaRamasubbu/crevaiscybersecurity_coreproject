﻿using System;
using System.Collections.Generic;

namespace DAPP_Model.EDMX_MySql
{
    public partial class Changelogdetails
    {
        public long Id { get; set; }
        public long? ChangeLogId { get; set; }
        public string FieldName { get; set; }
        public string PreviousValue { get; set; }
        public string NewValue { get; set; }

        public Changelogheader ChangeLog { get; set; }
    }
}
