﻿using System;
using System.Collections.Generic;

namespace DAPP_Model.EDMX_MySql
{
    public partial class Changelogheader
    {
        public Changelogheader()
        {
            Changelogdetails = new HashSet<Changelogdetails>();
        }

        public long Id { get; set; }
        public long? DeviceId { get; set; }
        public DateTime? CreatedOn { get; set; }

        public Devicedetails Device { get; set; }
        public ICollection<Changelogdetails> Changelogdetails { get; set; }
    }
}
