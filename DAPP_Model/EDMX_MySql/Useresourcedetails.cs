﻿using System;
using System.Collections.Generic;

namespace DAPP_Model.EDMX_MySql
{
    public partial class Useresourcedetails
    {
        public long Id { get; set; }
        public long? DeviceId { get; set; }
        public int? ResourceType { get; set; }
        public decimal? Ramrequired { get; set; }
        public decimal? StorageRequired { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public TimeSpan? RequiredTime { get; set; }

        public Devicedetails Device { get; set; }
        public Resourcetype ResourceTypeNavigation { get; set; }
    }
}
