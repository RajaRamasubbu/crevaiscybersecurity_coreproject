﻿using System;
using System.Collections.Generic;

namespace DAPP_Model.EDMX_MySql
{
    public partial class Resourcetype
    {
        public Resourcetype()
        {
            Shareresourcedetails = new HashSet<Shareresourcedetails>();
            Useresourcedetails = new HashSet<Useresourcedetails>();
        }

        public int Id { get; set; }
        public string ResourceType1 { get; set; }
        public bool? IsDeleted { get; set; }

        public ICollection<Shareresourcedetails> Shareresourcedetails { get; set; }
        public ICollection<Useresourcedetails> Useresourcedetails { get; set; }
    }
}
