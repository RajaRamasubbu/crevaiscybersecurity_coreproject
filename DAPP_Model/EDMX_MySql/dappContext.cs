﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace DAPP_Model.EDMX_MySql
{
    public partial class dappContext : DbContext
    {
        public dappContext()
        {
        }

        public dappContext(DbContextOptions<dappContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Changelogdetails> Changelogdetails { get; set; }
        public virtual DbSet<Changelogheader> Changelogheader { get; set; }
        public virtual DbSet<Devicedetails> Devicedetails { get; set; }
        public virtual DbSet<Errorlog> Errorlog { get; set; }
        public virtual DbSet<Resourcesharesetting> Resourcesharesetting { get; set; }
        public virtual DbSet<Resourcetype> Resourcetype { get; set; }
        public virtual DbSet<Shareresourcedetails> Shareresourcedetails { get; set; }
        public virtual DbSet<Useresourcedetails> Useresourcedetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if(!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build();
                var connectionString = configuration.GetConnectionString("DAPPEntities");
                optionsBuilder.UseMySql(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Changelogdetails>(entity =>
            {
                entity.ToTable("changelogdetails");

                entity.HasIndex(e => e.ChangeLogId)
                    .HasName("FK_ChangeLogDetails_ChangeLogHeader");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.ChangeLogId).HasColumnType("bigint(20)");

                entity.Property(e => e.FieldName).HasColumnType("varchar(250)");

                entity.Property(e => e.NewValue).HasColumnType("varchar(1500)");

                entity.Property(e => e.PreviousValue).HasColumnType("varchar(1500)");

                entity.HasOne(d => d.ChangeLog)
                    .WithMany(p => p.Changelogdetails)
                    .HasForeignKey(d => d.ChangeLogId)
                    .HasConstraintName("FK_ChangeLogDetails_ChangeLogHeader");
            });

            modelBuilder.Entity<Changelogheader>(entity =>
            {
                entity.ToTable("changelogheader");

                entity.HasIndex(e => e.DeviceId)
                    .HasName("FK_ChangeLogHeader_DeviceDetails");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.DeviceId).HasColumnType("bigint(20)");

                entity.HasOne(d => d.Device)
                    .WithMany(p => p.Changelogheader)
                    .HasForeignKey(d => d.DeviceId)
                    .HasConstraintName("FK_ChangeLogHeader_DeviceDetails");
            });

            modelBuilder.Entity<Devicedetails>(entity =>
            {
                entity.ToTable("devicedetails");

                entity.HasIndex(e => e.AgentId)
                    .HasName("IX_Table_AgentId");

                entity.HasIndex(e => e.HashId)
                    .HasName("IX_Table_HashId");

                entity.HasIndex(e => e.Macaddress)
                    .HasName("IX_Table_MACAddress")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.AgentId).HasColumnType("bigint(20)");

                entity.Property(e => e.AgentName).HasColumnType("varchar(250)");

                entity.Property(e => e.Biosid)
                    .HasColumnName("BIOSId")
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.CreatedBy).HasColumnType("varchar(250)");

                entity.Property(e => e.DeviceName).HasColumnType("varchar(250)");

                entity.Property(e => e.DeviceStatus).HasColumnType("int(11)");

                entity.Property(e => e.EriskDeviceId)
                    .HasColumnName("ERiskDeviceId")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.HashId).HasColumnType("varchar(250)");

                entity.Property(e => e.LastModifiedBy).HasColumnType("varchar(250)");

                entity.Property(e => e.Macaddress)
                    .HasColumnName("MACAddress")
                    .HasColumnType("varchar(250)");

                entity.Property(e => e.Processing).HasColumnType("decimal(18,3)");

                entity.Property(e => e.ProcessingMax)
                    .HasColumnName("ProcessingMAX")
                    .HasColumnType("decimal(18,3)");

                entity.Property(e => e.Storage).HasColumnType("decimal(18,3)");

                entity.Property(e => e.StorageMax)
                    .HasColumnName("StorageMAX")
                    .HasColumnType("decimal(18,3)");
            });

            modelBuilder.Entity<Errorlog>(entity =>
            {
                entity.ToTable("errorlog");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.Exception).HasColumnType("varchar(4000)");

                entity.Property(e => e.Level)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Logger)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasColumnType("varchar(4000)");

                entity.Property(e => e.Thread)
                    .IsRequired()
                    .HasColumnType("varchar(4000)");
            });

            modelBuilder.Entity<Resourcesharesetting>(entity =>
            {
                entity.ToTable("resourcesharesetting");

                entity.HasIndex(e => e.DeviceId)
                    .HasName("FK_ResourceShareSetting_DeviceDetails");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.CreatedBy).HasColumnType("varchar(250)");

                entity.Property(e => e.DeviceId).HasColumnType("bigint(20)");

                entity.Property(e => e.IsDeleted)
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.LastModifiedBy).HasColumnType("varchar(250)");

                entity.Property(e => e.TurnOffFor).HasColumnType("int(11)");

                entity.HasOne(d => d.Device)
                    .WithMany(p => p.Resourcesharesetting)
                    .HasForeignKey(d => d.DeviceId)
                    .HasConstraintName("FK_ResourceShareSetting_DeviceDetails");
            });

            modelBuilder.Entity<Resourcetype>(entity =>
            {
                entity.ToTable("resourcetype");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.IsDeleted).HasColumnType("bit(1)");

                entity.Property(e => e.ResourceType1)
                    .HasColumnName("ResourceType")
                    .HasColumnType("varchar(250)");
            });

            modelBuilder.Entity<Shareresourcedetails>(entity =>
            {
                entity.ToTable("shareresourcedetails");

                entity.HasIndex(e => e.DeviceId)
                    .HasName("FK_ShareResourceDetails_DeviceDetails");

                entity.HasIndex(e => e.ResourceType)
                    .HasName("FK_ShareResourceDetails_ResourceType");

                entity.HasIndex(e => e.SharedToDeviceId)
                    .HasName("shareresourcedetails_ShareTo_Devicedetails_Id");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.CreatedBy).HasColumnType("varchar(250)");

                entity.Property(e => e.DeviceId).HasColumnType("bigint(20)");

                entity.Property(e => e.IsShareStopped).HasColumnType("bit(1)");

                entity.Property(e => e.LastModifiedBy).HasColumnType("varchar(250)");

                entity.Property(e => e.ProcessingShared).HasColumnType("decimal(18,3)");

                entity.Property(e => e.ResourceType).HasColumnType("int(11)");

                entity.Property(e => e.ShareStoppedBy).HasColumnType("varchar(250)");

                entity.Property(e => e.SharedToDeviceId).HasColumnType("bigint(20)");

                entity.Property(e => e.StorageShared).HasColumnType("decimal(18,3)");

                entity.Property(e => e.TotalAvailability).HasColumnType("decimal(18,3)");

                entity.HasOne(d => d.Device)
                    .WithMany(p => p.ShareresourcedetailsDevice)
                    .HasForeignKey(d => d.DeviceId)
                    .HasConstraintName("FK_ShareResourceDetails_DeviceDetails");

                entity.HasOne(d => d.ResourceTypeNavigation)
                    .WithMany(p => p.Shareresourcedetails)
                    .HasForeignKey(d => d.ResourceType)
                    .HasConstraintName("FK_ShareResourceDetails_ResourceType");

                entity.HasOne(d => d.SharedToDevice)
                    .WithMany(p => p.ShareresourcedetailsSharedToDevice)
                    .HasForeignKey(d => d.SharedToDeviceId)
                    .HasConstraintName("shareresourcedetails_ShareTo_Devicedetails_Id");
            });

            modelBuilder.Entity<Useresourcedetails>(entity =>
            {
                entity.ToTable("useresourcedetails");

                entity.HasIndex(e => e.DeviceId)
                    .HasName("FK_UseResourceDetails_DeviceDetails");

                entity.HasIndex(e => e.ResourceType)
                    .HasName("FK_UseResourceDetails_ResourceType");

                entity.Property(e => e.Id).HasColumnType("bigint(20)");

                entity.Property(e => e.CreatedBy).HasColumnType("varchar(250)");

                entity.Property(e => e.DeviceId).HasColumnType("bigint(20)");

                entity.Property(e => e.Ramrequired)
                    .HasColumnName("RAMRequired")
                    .HasColumnType("decimal(18,2)");

                entity.Property(e => e.ResourceType).HasColumnType("int(11)");

                entity.Property(e => e.StorageRequired).HasColumnType("decimal(18,2)");

                entity.HasOne(d => d.Device)
                    .WithMany(p => p.Useresourcedetails)
                    .HasForeignKey(d => d.DeviceId)
                    .HasConstraintName("FK_UseResourceDetails_DeviceDetails");

                entity.HasOne(d => d.ResourceTypeNavigation)
                    .WithMany(p => p.Useresourcedetails)
                    .HasForeignKey(d => d.ResourceType)
                    .HasConstraintName("FK_UseResourceDetails_ResourceType");
            });
        }
    }
}
