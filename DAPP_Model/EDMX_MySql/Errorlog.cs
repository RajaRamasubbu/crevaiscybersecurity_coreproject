﻿using System;
using System.Collections.Generic;

namespace DAPP_Model.EDMX_MySql
{
    public partial class Errorlog
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public string Thread { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
    }
}
