﻿using System;
using System.Collections.Generic;

namespace DAPP_Model.EDMX_MySql
{
    public partial class Shareresourcedetails
    {
        public long Id { get; set; }
        public long? DeviceId { get; set; }
        public int? ResourceType { get; set; }
        public decimal? ProcessingShared { get; set; }
        public decimal? StorageShared { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public decimal? TotalAvailability { get; set; }
        public bool? IsShareStopped { get; set; }
        public string ShareStoppedBy { get; set; }
        public long? SharedToDeviceId { get; set; }

        public Devicedetails Device { get; set; }
        public Resourcetype ResourceTypeNavigation { get; set; }
        public Devicedetails SharedToDevice { get; set; }
    }
}
