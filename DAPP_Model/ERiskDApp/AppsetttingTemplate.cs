﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAPP_Model.ERiskDApp
{
    public class AppsetttingTemplate
    {
        public string contractAddress { get; set; }
        public string DefaultAccount { get; set; }

        public string privatekey { get; set; }
        public string URL { get; set; }
        public string GasPrice { get; set; }
        public string Gaslimit { get; set; }


        public string Shell_Host { get; set; }
        public string Shell_User { get; set; }
        public string Shell_Passwd { get; set; }
        public string Shell_cmd_Start { get; set; }
        public string Shell_cmd_Stop { get; set; }
        public bool Shell_IsProduction { get; set; }
    }
}
