﻿using System;
using System.Collections.Generic;

namespace DAPP_Model.ERiskDApp
{

    public class DeviceList
    {
        public int id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int agentId { get; set; }
        public string manufacturer { get; set; }
        public string os { get; set; }
        public string processor { get; set; }
        public string ram { get; set; }
        public string ip_address { get; set; }
        public string mac_address { get; set; }
        public string serial { get; set; }
        public string os_arch { get; set; }
        public object no_of_cors { get; set; }
        public int status { get; set; }
        public bool isDeleted { get; set; }
        public string digitalCertificate { get; set; }
        public string firmwareVersion { get; set; }
        public string certificateIssuer { get; set; }
        public string managementIp { get; set; }
        public string storage { get; set; }
        public string userId { get; set; }
        public int? createdById { get; set; }
        public int? updatedById { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
    }

    public class AgentUtilization
    {
        public string id { get; set; }
        public string agentId { get; set; }
        public decimal cpu { get; set; }
        public decimal memory { get; set; }
        public decimal cpuAvail { get; set; }
        public decimal memoryAvail { get; set; }
        public decimal cpuMax { get; set; }
        public decimal memoryMax { get; set; }
        public decimal storage { get; set; }
        public decimal storageMax { get; set; }
        public decimal storageAvail { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
    }

    public class AgentList
    {
        public object systemScanHour { get; set; }
        public object systemScanMinutes { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int? domainId { get; set; }
        public int updateId { get; set; }
        public bool eSenseEncryption { get; set; }
        public bool eSenseDisplayUtilization { get; set; }
        public bool backupPrimaryAgent { get; set; }
        public bool automaticUpdate { get; set; }
        public bool isDeleted { get; set; }
        public string agentStatus { get; set; }
        public string version { get; set; }
        public int status { get; set; }
        public int confidenceLevel { get; set; }
        public int learningTime { get; set; }
        public int auditRecordRetention { get; set; }
        public int hour { get; set; }
        public int minutes { get; set; }
        public bool isSystemScan { get; set; }
        public bool isAutoNotify { get; set; }
        public int organizationId { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string address { get; set; }
        public bool isQuarantined { get; set; }
        public int alertThreshold { get; set; }
        public object trustedDevices { get; set; }
        public int? createdById { get; set; }
        public int? updatedById { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
        public IList<DeviceList> device { get; set; }
        public IList<AgentUtilization> AgentUtilization { get; set; }
    }

    public class UserList
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public int numberOfDomain { get; set; }
        public string username { get; set; }
        public object apiUrl { get; set; }
        public int status { get; set; }
        public string password { get; set; }
        public int? createdById { get; set; }
        public int? updatedById { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
        public IList<AgentList> agent { get; set; }
    }

    //public class UserList
    //{
    //    public int id { get; set; }
    //    public string name { get; set; }
    //    public string address { get; set; }
    //    public int numberOfDomain { get; set; }
    //    public string username { get; set; }
    //    public string apiUrl { get; set; }
    //    public int status { get; set; }
    //    public string password { get; set; }
    //    public Nullable<int> createdById { get; set; }
    //    public Nullable<int> updatedById { get; set; }
    //    public DateTime createdAt { get; set; }
    //    public DateTime updatedAt { get; set; }
    //    public List<AgentList> agent { get; set; }
    //}

    //public class AgentList
    //{
    //    public int systemScanHour { get; set; }
    //    public int systemScanMinutes { get; set; }
    //    public int id { get; set; }
    //    public string name { get; set; }
    //    public string domainId { get; set; }
    //    public int updateId { get; set; }
    //    public bool eSenseEncryption { get; set; }
    //    public bool eSenseDisplayUtilization { get; set; }
    //    public bool backupPrimaryAgent { get; set; }
    //    public bool automaticUpdate { get; set; }
    //    public bool isDeleted { get; set; }
    //    public string agentStatus { get; set; }
    //    public string version { get; set; }
    //    public int status { get; set; }
    //    public int confidenceLevel { get; set; }
    //    public int learningTime { get; set; }
    //    public int auditRecordRetention { get; set; }
    //    public int hour { get; set; }
    //    public int minutes { get; set; }
    //    public bool isSystemScan { get; set; }
    //    public bool isAutoNotify { get; set; }
    //    public int organizationId { get; set; }
    //    public string latitude { get; set; }
    //    public string longitude { get; set; }
    //    public string address { get; set; }
    //    public bool isQuarantined { get; set; }
    //    public int alertThreshold { get; set; }
    //    public string trustedDevices { get; set; }
    //    public Nullable<int> createdById { get; set; }
    //    public Nullable<int> updatedById { get; set; }
    //    public DateTime createdAt { get; set; }
    //    public DateTime updatedAt { get; set; }
    //    public List<DeviceList> device { get; set; }
    //    public List<AgentUtilization> AgentUtilization { get; set; }
    //}

    //public class DeviceList
    //{
    //    public int id { get; set; }
    //    public string name { get; set; }
    //    public string type { get; set; }
    //    public int agentId { get; set; }
    //    public string manufacturer { get; set; }
    //    public string os { get; set; }
    //    public string processor { get; set; }
    //    public string ram { get; set; }
    //    public string ip_address { get; set; }
    //    public string mac_address { get; set; }
    //    public string serial { get; set; }
    //    public string os_arch { get; set; }
    //    public string no_of_cors { get; set; }
    //    public int status { get; set; }
    //    public bool isDeleted { get; set; }
    //    public string digitalCertificate { get; set; }
    //    public string firmwareVersion { get; set; }
    //    public string certificateIssuer { get; set; }
    //    public string managementIp { get; set; }
    //    public string storage { get; set; }
    //    public string userId { get; set; }
    //    public Nullable<int> createdById { get; set; }
    //    public Nullable<int> updatedById { get; set; }
    //    public DateTime createdAt { get; set; }
    //    public DateTime updatedAt { get; set; }
    //}


    //public class AgentUtilization
    //{
    //    public string id { get; set; }
    //    public string agentId { get; set; }
    //    public decimal cpu { get; set; }
    //    public decimal memory { get; set; }
    //    public decimal cpuAvail { get; set; }
    //    public decimal memoryAvail { get; set; }
    //    public decimal cpuMax { get; set; }
    //    public decimal memoryMax { get; set; }
    //    public decimal storage { get; set; }
    //    public decimal storageMax { get; set; }
    //    public decimal storageAvail { get; set; }
    //    public DateTime createdAt { get; set; }
    //    public DateTime updatedAt { get; set; }
    //}
    public class DevicesDetails
    {
        public int AgentId { get; set; }
        public string AgentName { get; set; }
        public string DeviceName { get; set; }
        public string mac_address { get; set; }
        public string RAM { get; set; }
        public string Storage { get; set; }
        public int Status { get; set; }
        public DateTime DeviceCreatedAt { get; set; }
        public DateTime DeviceUpdatedAt { get; set; }
    }

    public class LogDetails
    {
        public string Log { get; set; }
    }
}
