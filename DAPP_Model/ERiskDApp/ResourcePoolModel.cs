﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAPP_Model.ERiskDApp
{
    public class ResourcePoolModel  /*ShareResourceModel*/
    {
        public long DeviceId {get;set;}
        public List<DropdownList> DeviceNamelst { get; set; }
        public Nullable<decimal> TotalProcessingPower { get; set; }
        public Nullable<long> ShareProcessingPowerPercentage { get; set; }
        public Nullable<decimal> ShareProcessingPowerUnit { get; set; }
        public Nullable<decimal> TotalStorage { get; set; }
        public Nullable<long> ShareStoragePercentage { get; set; }
        public Nullable<decimal> ShareStorageUnit { get; set; }
        public long SharedToDeviceId { get; set; }
    }

    public class ShareResourceSession
    {
        public string DeviceName { get; set; }
        public Nullable<int> ResourceType { get; set; }
        public Nullable<decimal> TotalAvailability { get; set; }
        public Nullable<decimal> ShareResource { get; set; }
    }

    public class ShareResourceGrid
    {
        public long Id { get; set; }
        public string DeviceName { get; set; }
        public string ResourceType { get; set; }
        public string TotalAvailability { get; set; }
        public string ShareResource { get; set; }
        public bool ShareStatus { get; set; }
        public String SharedToDeviceName { get; set; }
    }

    public class UseResourceModel
    {
        public List<string> ProcDeviceName { get; set; }
        public Nullable<decimal> AvailableRAM { get; set; }
        public Nullable<decimal> ProcTotalRequirement { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{00:00:00}", ApplyFormatInEditMode = true)]
        public Nullable<TimeSpan> ProcDurationNeeded { get; set; }
        public List<string> StorDeviceName { get; set; }
        public Nullable<decimal> AvailableStorage { get; set; }
        public Nullable<decimal> StorTotalRequirement { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{00:00:00}", ApplyFormatInEditMode = true)]
        public Nullable<TimeSpan> StorDurationNeeded { get; set; }
    }

    public class DashboardModel
    {
        public long DeviceId { get; set; }
        public List<DropdownList> DeviceNamelst { get; set; }
        public decimal? RAMAvailabity { get; set; }
        public decimal? RAMUtilization { get; set; }
        public decimal? RAMSurplus { get; set; }
        public decimal? RAMShortfall { get; set; }

        public decimal? StoAvailabity { get; set; }
        public decimal? StoUtilization { get; set; }
        public decimal? StoSurplus { get; set; }
        public decimal? StoShortfall { get; set; }

        public decimal? HashID { get; set; }
        public decimal? Utilization { get; set; }
        public decimal? SessionLength { get; set; }
        public decimal? StartTime { get; set; }
    }

}
