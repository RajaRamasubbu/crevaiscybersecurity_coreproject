﻿using DAPP_Model.EDMX_MySql;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;


namespace DAPP_Model.ERiskDApp
{
    public static class CommonClass
    {

        const string DataContextKey = "DAPPContext";
        public static dappContext DB
        {
            get
            {
                //if (HttpContext.Current.Items[DataContextKey] == null)
                //    HttpContext.Current.Items[DataContextKey] = new DAPPContext();
                //return (DAPPContext)HttpContext.Current.Items[DataContextKey];

                return new dappContext();
            }
        }
        public class CommonIdTitle
        {
            public long Id { get; set; }
            public string Title { get; set; }
        }

        public static List<CommonIdTitle> DeviceName()
        {
            return DB.Devicedetails.Select(x => new CommonIdTitle { Id = x.Id, Title = x.DeviceName }).ToList();
        }

        public static List<CommonIdTitle> ResourceType()
        {
            return DB.Resourcetype.Select(x => new CommonIdTitle { Id = x.Id, Title = x.ResourceType1 }).ToList();
        }
    }

    public class DropdownList
    {
        public long Id { get; set; }
        public string Text { get; set; }
    }
}
