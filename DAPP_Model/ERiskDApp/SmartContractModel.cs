﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace DAPP_Model.ERiskDApp
{
    public class SmartContractModel
    {
        public long DeviceId { get; set; }
        public string Information { get; set; }
    }

    public class ResponseMessage
    {
        public HttpStatusCode Status { get; set; }
        public string HashId { get; set; }
    }
}
