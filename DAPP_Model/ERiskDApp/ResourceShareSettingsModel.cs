﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAPP_Model.ERiskDApp
{
    public class ResourceShareSettingsModel
    {
        public long DeviceId { get; set; }
        public int TurnOffFor { get; set; }
        public List<DropdownList> DeviceNamelst { get; set; }

        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:HH:mm:ss}", ApplyFormatInEditMode = true)]
        public Nullable<TimeSpan> TurnOffFrom { get; set; }

        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{HH:mm:ss}}", ApplyFormatInEditMode = true)]
        public Nullable<TimeSpan> TurnOffTo { get; set; }
    }
}
