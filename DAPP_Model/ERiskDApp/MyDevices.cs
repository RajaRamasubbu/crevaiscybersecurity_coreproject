﻿namespace DAPP_Model.ERiskDApp
{
    public class MyDevices
    {
        public int TotalDevices { get; set; }
        public int OnBlockChain { get; set; }
        public int InProgress { get; set; }
    }

    //public class DevicesGridMdel
    //{
    //    public int Id { get; set; }
    //    public string HashId { get; set; }
    //    public string AgentId { get; set; }
    //    public string MacId { get; set; }
    //    public string BiosId { get; set; }
    //    public DateTime TimeStamp { get; set; }
    //}
}
