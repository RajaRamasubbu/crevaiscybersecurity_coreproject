﻿using System;

namespace DAPP_Model.ERiskDApp
{
    public class SmartContractAPIModel
    {
        public string UserName { get; set; }
        public string MacAddress { get; set; }
        public long AgentId { get; set; }
        public string AgentName { get; set; }
        public long DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string Processing { get; set; }
        public string ProcessingMAX { get; set; }
        public string Storage { get; set; }
        public string StorageMAX { get; set; }
        public int DeviceStatus { get; set; }
        public Nullable<DateTime> DeviceCreatedAt { get; set; }
        public Nullable<DateTime> DeviceUpdatedAt { get; set; }
        public string BiosId { get; set; }

    }
}
