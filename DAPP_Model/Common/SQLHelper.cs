﻿using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace DAPP_Model.Common
{
    public class SQLHelper
    {

        #region "FILL DATA TABLE"

        public static void Fill(DataTable dataTable, String procedureName, string ConnectionString)
        {
            MySqlConnection oConnection = new MySqlConnection(ConnectionString);
            MySqlCommand oCommand = new MySqlCommand(procedureName, oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            MySqlDataAdapter oAdapter = new MySqlDataAdapter();

            oAdapter.SelectCommand = oCommand;
            oConnection.Open();
            using (MySqlTransaction oTransaction = oConnection.BeginTransaction())
            {
                try
                {
                    oAdapter.SelectCommand.Transaction = oTransaction;
                    oAdapter.Fill(dataTable);
                    oTransaction.Commit();
                }
                catch
                {
                    oTransaction.Rollback();
                    throw;
                }
                finally
                {
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                    oConnection.Dispose();
                    oAdapter.Dispose();
                }
            }
        }

        public static void Fill(DataTable dataTable, String procedureName, MySqlParameter[] parameters, string ConnectionString)
        {
            MySqlConnection oConnection = new MySqlConnection(ConnectionString);
            MySqlCommand oCommand = new MySqlCommand(procedureName, oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
                oCommand.Parameters.AddRange(parameters);

            MySqlDataAdapter oAdapter = new MySqlDataAdapter();

            oAdapter.SelectCommand = oCommand;
            oConnection.Open();
            using (MySqlTransaction oTransaction = oConnection.BeginTransaction())
            {
                try
                {
                    oAdapter.SelectCommand.Transaction = oTransaction;
                    oAdapter.Fill(dataTable);
                    oTransaction.Commit();
                }
                catch
                {
                    oTransaction.Rollback();
                    throw;
                }
                finally
                {
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                    oConnection.Dispose();
                    oAdapter.Dispose();
                }
            }
        }

        public static void Fill(DataTable dataTable, String procedureName, MySqlParameter[] parameters, MySqlTransaction oTransaction, MySqlConnection oConnection)
        {
            MySqlCommand oCommand = new MySqlCommand(procedureName, oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
                oCommand.Parameters.AddRange(parameters);

            MySqlDataAdapter oAdapter = new MySqlDataAdapter();
            oAdapter.SelectCommand = oCommand;

            try
            {
                oAdapter.SelectCommand.Transaction = oTransaction;
                oAdapter.Fill(dataTable);
            }
            catch
            {
                throw;
            }
            finally
            {
                oAdapter.Dispose();
            }

        }

        #endregion

        #region "FILL DATASET"

        public static void Fill(DataSet dataSet, String procedureName, string ConnectionString)
        {
            MySqlConnection oConnection = new MySqlConnection(ConnectionString);
            MySqlCommand oCommand = new MySqlCommand(procedureName, oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            MySqlDataAdapter oAdapter = new MySqlDataAdapter();

            oAdapter.SelectCommand = oCommand;
            oConnection.Open();
            using (MySqlTransaction oTransaction = oConnection.BeginTransaction())
            {
                try
                {
                    oAdapter.SelectCommand.Transaction = oTransaction;
                    oAdapter.Fill(dataSet);
                    oTransaction.Commit();
                }
                catch
                {
                    oTransaction.Rollback();
                    throw;
                }
                finally
                {
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                    oConnection.Dispose();
                    oAdapter.Dispose();
                }
            }
        }

        public static void Fill(DataSet dataSet, String procedureName, MySqlParameter[] parameters, string ConnectionString)
        {
            MySqlConnection oConnection = new MySqlConnection(ConnectionString);
            MySqlCommand oCommand = new MySqlCommand(procedureName, oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
                oCommand.Parameters.AddRange(parameters);

            MySqlDataAdapter oAdapter = new MySqlDataAdapter();

            oAdapter.SelectCommand = oCommand;
            oConnection.Open();
            using (MySqlTransaction oTransaction = oConnection.BeginTransaction())
            {
                try
                {
                    oAdapter.SelectCommand.Transaction = oTransaction;
                    oAdapter.Fill(dataSet);
                    oTransaction.Commit();
                }
                catch
                {
                    oTransaction.Rollback();
                    throw;
                }
                finally
                {
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                    oConnection.Dispose();
                    oAdapter.Dispose();
                }
            }
        }

        public static void Fill(DataSet dataSet, String procedureName, MySqlParameter[] parameters, MySqlTransaction oTransaction, MySqlConnection oConnection)
        {
            //  SqlConnection oConnection = new SqlConnection(ConnectionString);
            MySqlCommand oCommand = new MySqlCommand(procedureName, oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            if (parameters != null)
                oCommand.Parameters.AddRange(parameters);

            MySqlDataAdapter oAdapter = new MySqlDataAdapter();

            oAdapter.SelectCommand = oCommand;
            //oConnection.Open();
            //using (SqlTransaction oTransaction = oConnection.BeginTransaction())
            //{
            try
            {
                oAdapter.SelectCommand.Transaction = oTransaction;
                oAdapter.Fill(dataSet);
                // oTransaction.Commit();
            }
            catch
            {
                // oTransaction.Rollback();
                throw;
            }
            finally
            {
                //if (oConnection.State == ConnectionState.Open)
                //    oConnection.Close();
                //oConnection.Dispose();
                oAdapter.Dispose();
            }
        }


        #endregion

        #region "EXECUTE SCALAR"

        public static object ExecuteScalar(String procedureName, string ConnectionString)
        {
            MySqlConnection oConnection = new MySqlConnection(ConnectionString);
            MySqlCommand oCommand = new MySqlCommand(procedureName, oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            object oReturnValue;
            oConnection.Open();
            using (MySqlTransaction oTransaction = oConnection.BeginTransaction())
            {
                try
                {
                    oCommand.Transaction = oTransaction;
                    oReturnValue = oCommand.ExecuteScalar();
                    oTransaction.Commit();
                }
                catch
                {
                    oTransaction.Rollback();
                    throw;
                }
                finally
                {
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                    oConnection.Dispose();
                    oCommand.Dispose();
                }
            }
            return oReturnValue;
        }

        public static object ExecuteScalar(String procedureName, MySqlParameter[] parameters, string ConnectionString)
        {
            MySqlConnection oConnection = new MySqlConnection(ConnectionString);
            MySqlCommand oCommand = new MySqlCommand(procedureName, oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            object oReturnValue;
            oConnection.Open();
            using (MySqlTransaction oTransaction = oConnection.BeginTransaction())
            {
                try
                {
                    if (parameters != null)
                        oCommand.Parameters.AddRange(parameters);

                    oCommand.Transaction = oTransaction;
                    oReturnValue = oCommand.ExecuteScalar();
                    oTransaction.Commit();
                }
                catch
                {
                    oTransaction.Rollback();
                    throw;
                }
                finally
                {
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                    oConnection.Dispose();
                    oCommand.Dispose();
                }
            }
            return oReturnValue;
        }

        #endregion

        #region "EXECUTE NON QUERY"

        public static void ExecuteNonQuery(string procedureName, string ConnectionString)
        {
            MySqlConnection oConnection = new MySqlConnection(ConnectionString);
            MySqlCommand oCommand = new MySqlCommand(procedureName, oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            //int iReturnValue;
            oConnection.Open();
            using (MySqlTransaction oTransaction = oConnection.BeginTransaction())
            {
                try
                {
                    oCommand.Transaction = oTransaction;
                    //iReturnValue = 
                    oCommand.ExecuteNonQuery();
                    oTransaction.Commit();
                }
                catch
                {
                    oTransaction.Rollback();
                    throw;
                }
                finally
                {
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                    oConnection.Dispose();
                    oCommand.Dispose();
                }
            }
            // return iReturnValue;
        }

        public static void ExecuteNonQuery(string procedureName, MySqlParameter[] parameters, string ConnectionString)
        {
            MySqlConnection oConnection = new MySqlConnection(ConnectionString);
            MySqlCommand oCommand = new MySqlCommand(procedureName, oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            // int iReturnValue;
            oConnection.Open();
            using (MySqlTransaction oTransaction = oConnection.BeginTransaction())
            {
                try
                {
                    if (parameters != null)
                        oCommand.Parameters.AddRange(parameters);

                    oCommand.Transaction = oTransaction;
                    //iReturnValue = 
                    oCommand.ExecuteNonQuery();
                    oTransaction.Commit();
                }
                catch
                {
                    oTransaction.Rollback();
                    throw;
                }
                finally
                {
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                    oConnection.Dispose();
                    oCommand.Dispose();
                }
            }
            // return iReturnValue;
        }

        public static void ExecuteNonQuery(string procedureName, MySqlParameter[] parameters, MySqlTransaction oTransaction, MySqlConnection oConnection)
        {
            MySqlCommand oCommand = new MySqlCommand(procedureName, oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                if (parameters != null)
                    oCommand.Parameters.AddRange(parameters);

                oCommand.Transaction = oTransaction;
                oCommand.ExecuteNonQuery();
                //oTransaction.Commit();
            }
            catch
            {
                throw;
            }
            finally
            {
                oCommand.Dispose();
            }
        }

        #endregion

    }
}
