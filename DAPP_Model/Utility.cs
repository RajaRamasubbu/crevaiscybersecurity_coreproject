﻿
using DAPP_Model.ERiskDApp;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.Extensions.Configuration;
using System.Configuration;
using System.IO;

namespace DAPP_Model
{
    public class Utility
    {

        public static string ApplicationExeDirectory()
        {
            var location = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var appRoot = Path.GetDirectoryName(location);

            return appRoot;
        }

        //Get the connection string from Web config file.  
        public static string GetConnectionString()
        {

            string applicationExeDirectory = ApplicationExeDirectory();

            var builder = new ConfigurationBuilder()
            .SetBasePath(applicationExeDirectory)
            .AddJsonFile("appsettings.json");

            return builder.Build().GetConnectionString("DAPPEntities");

            //string returnValue = null;

            //HostingEnvironment hostingenvironment = new HostingEnvironment();
            //IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().SetBasePath(hostingenvironment.ContentRootPath).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            //IConfigurationRoot configurationRoot = configurationBuilder.Build();
            //string connectionstrng = configurationRoot.GetConnectionString("DAPPEntities");
            //if (connectionstrng != null && connectionstrng != "")
            //    returnValue = connectionstrng;
            //else
            //    returnValue = null;

            //return returnValue;
        }
    }

    public class GetAppsettingTemplate
    {
        public AppsetttingTemplate appsetting { get; set; }

        public static GetAppsettingTemplate GetAppsetting()
        {
            //HostingEnvironment hostingenvironment = new HostingEnvironment();

            //var builder = new ConfigurationBuilder().SetBasePath(hostingenvironment.ContentRootPath).AddJsonFile("AppsettingTemplate.json", optional: true, reloadOnChange: true).Build();
            ////var builder = new ConfigurationBuilder().AddJsonFile(path + "\\Email Template\\" + Jsonname + ".json").Build();


            var location = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var appRoot = Path.GetDirectoryName(location);

            var builder = new ConfigurationBuilder()
            .SetBasePath(appRoot)
            .AddJsonFile("AppsettingTemplate.json").Build();

            var appsetting = new GetAppsettingTemplate();
            builder.Bind(appsetting);

            return appsetting;
        }
    }
}
