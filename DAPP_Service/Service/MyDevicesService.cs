﻿using DAPP_Model.EDMX_MySql;
using DAPP_Model.ERiskDApp;
using DAPP_Service.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;
using MySql.Data.MySqlClient;
using DAPP_Logger;
using DAPP_Model.Common;
using System.Data;
using DAPP_Model;

namespace DAPP_Service.Service
{
    public class MyDevicesService : IMyDevicesService, IDisposable
    {
        bool disposed = false;


        ~MyDevicesService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {

                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }

        public bool UpdateDeviceDetails(string UserName, UserList Users)
        {
            try
            {
                Devicedetails Device = new Devicedetails();
                List<Devicedetails> DeviceDetailsDB = new List<Devicedetails>();
                List<Devicedetails> DeviceDetailsApi = new List<Devicedetails>();
                Changelogheader LogHeader = new Changelogheader();
                List<Changelogdetails> LogdetailsLst = new List<Changelogdetails>();
                Changelogdetails LogDetails = new Changelogdetails();

                foreach (var item in Users.agent.Where(x => x.device != null).SelectMany(x => x.device).ToList())
                {
                    Device = new Devicedetails();
                    Device.AgentId = item.agentId;
                    Device.AgentName = Users.agent.Where(x => x.id == item.agentId).Select(x => x.name).FirstOrDefault();
                    Device.DeviceName = item.name;
                    Device.Macaddress = item.mac_address;
                    //string CPUVal = Regex.Replace(item.ram, @"[^\d]", "");
                    //Device.Cpu = (CPUVal == "" ? 0 : Convert.ToDecimal(CPUVal));
                    //Device.Storage = (item.storage == "" ? 0 : Convert.ToDecimal(item.storage));
                    Device.DeviceStatus = item.status;
                    Device.DeviceCreatedAt = item.createdAt;
                    Device.DeviceUpdatedAt = item.updatedAt;
                    Device.CreatedOn = DateTime.Now;
                    Device.CreatedBy = UserName.ToString();
                    Device.EriskDeviceId = item.id;
                    Device.Biosid = item.serial;
                    var agentutilization = Users.agent.SelectMany(x => x.AgentUtilization).Where(x => Convert.ToInt64(x.agentId) == item.agentId).FirstOrDefault();
                    if (agentutilization != null)
                    {
                        Device.ProcessingMax = agentutilization.memoryMax;
                        Device.Processing = agentutilization.memoryAvail;
                        Device.Storage = agentutilization.storageAvail;
                        Device.StorageMax = agentutilization.storageMax;
                    }
                    DeviceDetailsApi.Add(Device);
                }

                DeviceDetailsDB = GetAllDevicesListByUN(UserName);
                var differentRecord = DeviceDetailsApi.Where(x => !DeviceDetailsDB.Any(x1 => x1.CreatedBy == x.CreatedBy && x1.Macaddress == x.Macaddress)).ToList();
                var sameRecord = DeviceDetailsApi.Where(x => DeviceDetailsDB.Any(x1 => x1.CreatedBy == x.CreatedBy && x1.Macaddress == x.Macaddress)).ToList();
                if (differentRecord.Count > 0)
                {
                    foreach (var item in differentRecord)
                    {
                        using (var context = new dappContext())
                        {
                            using (IDbContextTransaction Tran = context.Database.BeginTransaction())
                            {
                                try
                                {
                                    context.Devicedetails.Add(item);
                                    context.SaveChanges();
                                    Tran.Commit();
                                }
                                catch (MySqlException e)
                                {
                                    Tran.Rollback();
                                    Logger.Error(e, "UpdateDeviceDetails DifferentRecord Loop");
                                    //throw e;
                                }
                            }
                        }
                    }
                }
                if (sameRecord.Count > 0)
                {
                    foreach (var item in sameRecord)
                    {
                        using (var context = new dappContext())
                        {
                            using (IDbContextTransaction Tran = context.Database.BeginTransaction())
                            {
                                try
                                {
                                    Device = DeviceAlreadyExistByMA(item.CreatedBy, item.Macaddress);
                                    if (Device.AgentId != item.AgentId || Device.Processing != item.Processing || Device.Storage != item.Storage)
                                    {
                                        if (Device.AgentId != item.AgentId)
                                        {
                                            LogHeader = new Changelogheader();
                                            LogDetails = new Changelogdetails();
                                            LogdetailsLst = new List<Changelogdetails>();

                                            LogHeader.DeviceId = Device.Id;
                                            LogHeader.CreatedOn = DateTime.Now;
                                            LogDetails.FieldName = "AgentName";
                                            LogDetails.PreviousValue = Device.AgentName.ToString();
                                            LogDetails.NewValue = item.AgentName.ToString();
                                            LogdetailsLst.Add(LogDetails);

                                            LogHeader.Changelogdetails = LogdetailsLst;
                                            context.Changelogheader.Add(LogHeader);
                                        }
                                        ////UpdateDeviceDetails agentid
                                        //Device.AgentId = item.AgentId;
                                        //Device.LastModifiedBy = UserName;
                                        //Device.LastModifiedOn = DateTime.Now;

                                        //Device.Cpumax = item.Cpumax;
                                        //Device.Cpu = item.Cpu;
                                        //Device.Storage = item.Storage;
                                        //Device.StorageMax = item.StorageMax;

                                        var CurrentDeviceDetails = context.Devicedetails.Where(x => x.Macaddress == item.Macaddress && x.CreatedBy == UserName);
                                        foreach (Devicedetails d in CurrentDeviceDetails)
                                        {
                                            d.AgentId = item.AgentId;
                                            d.LastModifiedBy = UserName;
                                            d.LastModifiedOn = DateTime.Now;
                                            d.ProcessingMax = item.ProcessingMax;
                                            d.Processing = item.Processing;
                                            d.Storage = item.Storage;
                                            d.StorageMax = item.StorageMax;
                                            d.DeviceCreatedAt = item.DeviceCreatedAt;
                                            d.DeviceUpdatedAt = item.DeviceUpdatedAt;
                                        }
                                    }

                                    context.SaveChanges();
                                    Tran.Commit();
                                }
                                catch (MySqlException e)
                                {
                                    Tran.Rollback();
                                    Logger.Error(e, "UpdateDeviceDetails Same Record Loop");
                                    // throw e;
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "UpdateDeviceDetails Method");
                throw e;
            }
        }

        public List<Devicedetails> GetAllDevicesListByUN(string UserName)
        {
            try
            {
                return CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == UserName).ToList();
                //if (DeviceCount > 0)
                //    return true;
                //else
                //    return false;

            }
            catch (Exception e)
            {
                Logger.Error(e, "GetAllDevicesListByUN Method");
                throw e;
            }
        }

        public List<Devicedetails> GetAllDevicesList()
        {
            try
            {
                return CommonClass.DB.Devicedetails.ToList();
                //if (DeviceCount > 0)
                //    return true;
                //else
                //    return false;

            }
            catch (Exception e)
            {
                Logger.Error(e, "GetAllDevicesList Method");
                throw e;
            }
        }

        public Devicedetails DeviceAlreadyExist(string username, string macaddress)
        {
            try
            {
                return CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == username && x.Macaddress == macaddress).FirstOrDefault();

            }
            catch (Exception e)
            {
                Logger.Error(e, "DeviceAlreadyExist Method");
                throw e;
            }
        }


        public Devicedetails DeviceAlreadyExistByMA(string UserName, string MacAddress)
        {
            try
            {
                return CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == UserName && x.Macaddress == MacAddress).FirstOrDefault();
                //if (DeviceCount > 0)
                //    return true;
                //else
                //    return false;

            }
            catch (Exception e)
            {
                Logger.Error(e, "DeviceAlreadyExistByMA Method");
                throw e;
            }
        }

        public long CheckForAgent(string UserName, string MacAddress, long AgentId)
        {
            try
            {
                List<DevicesDetails> devices = new List<DevicesDetails>();
                int DeviceCount = CommonClass.DB.Devicedetails.Where(x => x.AgentId == AgentId).Count();
                if (DeviceCount > 0)
                {
                    return 0;
                }
                else
                {
                    long CurrentAgentId = Convert.ToInt64(devices.Select(x => x.AgentId));
                    return CurrentAgentId;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CheckForAgent Method");
                throw e;
            }
        }

        public async Task<string> GetHashIdForNewDevice(SmartContractAPIModel model)
        {
            using (var context = new dappContext())
            {
                using (IDbContextTransaction Tran = context.Database.BeginTransaction())
                {
                    try
                    {
                        string Data = JsonConvert.SerializeObject(model);

                        using (InvokeSmartContract smartcontract = new InvokeSmartContract())
                        {
                            Devicedetails Device = new Devicedetails();
                            Device.AgentId = model.AgentId;
                            Device.AgentName = model.AgentName;
                            Device.DeviceName = model.DeviceName;
                            Device.Macaddress = model.MacAddress;
                            Device.Processing = Convert.ToDecimal(model.Processing);
                            Device.Storage = Convert.ToDecimal(model.Storage);
                            Device.DeviceStatus = model.DeviceStatus;
                            Device.DeviceCreatedAt = model.DeviceCreatedAt;
                            Device.DeviceUpdatedAt = model.DeviceUpdatedAt;
                            Device.CreatedOn = DateTime.Now;
                            Device.CreatedBy = model.UserName;
                            Device.Biosid = model.BiosId;
                            Device.StorageMax = Convert.ToDecimal(model.StorageMAX);
                            Device.ProcessingMax = Convert.ToDecimal(model.ProcessingMAX);
                            Device.HashId = Convert.ToString(await smartcontract.GethashIdForDevice(new SmartContractModel { DeviceId = model.DeviceId, Information = Data }));

                            if (Device.HashId != null)
                            {
                                context.Devicedetails.Add(Device);
                                return Device.HashId;
                            }
                            else
                            {
                                Tran.Rollback();
                                return null;
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        Tran.Rollback();
                        Logger.Error(e, "GetHashIdForNewDevice Method");
                        return null;
                        // throw e;
                    }
                    finally
                    {
                        context.SaveChanges();
                        Tran.Commit();
                    }
                }
            }
        }

        public List<string> GetLogList(long? DeviceId)
        {
            try
            {
                DataTable dt = new DataTable();
                MySqlParameter[] objParameter = new MySqlParameter[1];
                objParameter[0] = new MySqlParameter("@DeviceId", DeviceId);
                SQLHelper.Fill(dt, "SP_GetLogDetails", objParameter, Utility.GetConnectionString());

                List<string> loglist = new List<string>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    loglist.Add(Convert.ToString(dt.Rows[i][0]));
                }
                return loglist;
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetLogList Method");
                throw e;
            }
        }

        public void GenerateHashIdForDevices(string UserId)
        {
            try
            {

                if (GlobalValues.ThreadList.Where(x => x.ThreadUserId == UserId).Count() == 0)
                {
                    var Devicelist = CommonClass.DB.Devicedetails.Where(x => x.CreatedBy == UserId && x.HashId == null).ToList();
                    Action act = () => UpdateHashIdAgainstDeviceDetails(UserId, Devicelist);
                    Thread SCThread = new Thread(new ThreadStart(act));
                    SCThread.Name = UserId; //using the userid as the process name to avoid the duplicate calls 
                    SCThread.IsBackground = true;
                    SCThread.Start();

                    //adding the thread from global collection
                    GlobalValues.ThreadList.Add(new ThreadDetails { ThreadUserId = UserId });

                }

            }
            catch (Exception e)
            {
                Logger.Error(e, "GenerateHashIdForDevices Method");
                throw e;
            }
        }

        //method to test the thread 
        public void whileloop(string UserId)
        {
            try
            {
                while (UserId != null)
                {
                    Console.WriteLine("Running");
                }
            }
            catch (Exception E)
            {
                Logger.Error(E, "whileloop Method");
                throw E;
            }


        }

        public async void UpdateHashIdAgainstDeviceDetails(string UserId, List<Devicedetails> DeviceList)
        {
            // var DeviceList = CommonClass.DB.DeviceDetails.Where(x => x.CreatedBy == UserId && x.HashId == null);
            foreach (var model in DeviceList)
            {
                using (var context = new dappContext())
                {
                    using (IDbContextTransaction Tran = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var Device = context.Devicedetails.Where(x => x.Id == model.Id).FirstOrDefault();

                            var Data = JsonConvert.SerializeObject(new SmartContractAPIModel
                            {
                                AgentId = Convert.ToInt64(Device.AgentId),
                                AgentName = Device.AgentName,
                                BiosId = Device.Biosid,
                                DeviceCreatedAt = Device.DeviceCreatedAt,
                                DeviceId = Convert.ToInt64(Device.EriskDeviceId),
                                DeviceName = Device.DeviceName,
                                DeviceStatus = Convert.ToInt32(Device.DeviceStatus),
                                DeviceUpdatedAt = Device.DeviceUpdatedAt,
                                MacAddress = Device.Macaddress,
                                Processing = Convert.ToString(Device.Processing),
                                Storage = Convert.ToString(Device.Storage),
                                ProcessingMAX = Convert.ToString(Device.ProcessingMax),
                                StorageMAX = Convert.ToString(Device.StorageMax),
                                UserName = Device.CreatedBy
                            });

                            using (InvokeSmartContract smartcontract = new InvokeSmartContract())
                            {
                                if (Device.HashId == null)
                                    Device.HashId = Convert.ToString(await smartcontract.GethashIdForDevice(new SmartContractModel { DeviceId = model.Id, Information = Data }));

                            }

                        }
                        catch (Exception e)
                        {
                            Tran.Rollback();
                            Logger.Error(e, "UpdateHashIdAgainstDeviceDetails Method");
                        }
                        finally
                        {
                            context.SaveChanges();
                            Tran.Commit();
                        }
                    }
                }
            }
            //Removing the thread from global collection
            GlobalValues.ThreadList.Remove(GlobalValues.ThreadList.Where(x => x.ThreadUserId == UserId).FirstOrDefault());
        }
    }
}
