﻿using DAPP_Logger;
using DAPP_Model;
using DAPP_Service.Interface;
using Renci.SshNet;
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace DAPP_Service.Service
{
    public class InvokeShellScripts : IInvokeShellScripts, IDisposable
    {
        bool disposed = false;

        ~InvokeShellScripts()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {

                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }


        public bool RunShellScriptCommand(bool Isstart, string ResourceType, string HashId = null, decimal? SharedResource = null)
        {
            try
            {
                var setting = GetAppsettingTemplate.GetAppsetting();
                string Shell_Host = setting.appsetting.Shell_Host;
                string Shell_User = setting.appsetting.Shell_User;
                string Shell_Passwd = setting.appsetting.Shell_Passwd;
                string Shell_cmd_Start = setting.appsetting.Shell_cmd_Start;
                string Shell_cmd_Stop = setting.appsetting.Shell_cmd_Stop;
                bool Shell_IsProduction = setting.appsetting.Shell_IsProduction;

                SshCommand cmd;
                SshClient sshClient;
                if (Shell_IsProduction == true)
                {
                    //getting the private key from .pem file as string and converting to key
                    string key = File.ReadAllText(@"wwwroot/PrivateKeyFile/dapp-server.pem");
                    Regex removeSubjectRegex = new Regex("Subject:.*[\r\n]+", RegexOptions.IgnoreCase);
                    key = removeSubjectRegex.Replace(key, "");
                    MemoryStream buf = new MemoryStream(Encoding.UTF8.GetBytes(key));
                    PrivateKeyFile privateKeyFile = new PrivateKeyFile(buf);

                    PrivateKeyConnectionInfo privateKeyConnectionInfo = new PrivateKeyConnectionInfo(Shell_Host, Shell_User, privateKeyFile);
                    sshClient = new SshClient(privateKeyConnectionInfo);
                }
                else
                {
                    PasswordConnectionInfo connInfo = new PasswordConnectionInfo(Shell_Host, Shell_User, Shell_Passwd);
                    sshClient = new SshClient(connInfo);
                }

                // Console.WriteLine("going to start");
                sshClient.Connect();
                //Console.WriteLine("SSh COnnected");
                if (Isstart == true)
                {
                    if (ResourceType == "Storage")
                        cmd = sshClient.RunCommand(Shell_cmd_Start + " " + Convert.ToString(HashId) + " " + Convert.ToString(SharedResource));
                    else
                        cmd = sshClient.RunCommand(Shell_cmd_Start + " " + Convert.ToString(HashId) + " " + Convert.ToString(SharedResource));
                }
                else
                    cmd = sshClient.RunCommand(Shell_cmd_Stop + " " + Convert.ToString(HashId));

                //Console.WriteLine("command executed");
                string answer = cmd.Result;
                //Console.WriteLine("going to disconnect");
                sshClient.Disconnect();
                //Console.WriteLine("disconnected");
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "RunShellScriptCommand Method");
                return false;

            }
        }
    }
}
