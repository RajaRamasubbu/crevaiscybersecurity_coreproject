﻿using DAPP_Logger;
using DAPP_Model;
using DAPP_Model.Common;
using DAPP_Model.EDMX_MySql;
using DAPP_Model.ERiskDApp;
using DAPP_Service.Interface;
using Microsoft.EntityFrameworkCore.Storage;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAPP_Service.Service
{
    public class ResourcePoolService : IResourcePoolService, IDisposable
    {
        bool disposed = false;

        ~ResourcePoolService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {

                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }
        public bool ShareProcessingUpdate(long DeviceId, decimal TotalAvailabilty, decimal RAMShared, string Username, long ShareToDeviceId)
        {

            using (MySqlConnection oConnection = new MySqlConnection(Utility.GetConnectionString()))
            {
                oConnection.Open();
                using (MySqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    try
                    {
                        string HashId = CommonClass.DB.Devicedetails.Where(x => x.Id == DeviceId).Select(x => x.HashId).FirstOrDefault();

                        MySqlParameter[] objParameter = new MySqlParameter[10];
                        objParameter[0] = new MySqlParameter("DeviceId", DeviceId);
                        objParameter[1] = new MySqlParameter("ResourceType", 1);
                        objParameter[2] = new MySqlParameter("ProcessingShared", RAMShared * 1024);
                        objParameter[3] = new MySqlParameter("StorageShared", DBNull.Value);
                        objParameter[4] = new MySqlParameter("CreatedBy", Username);
                        objParameter[5] = new MySqlParameter("CreatedOn", DateTime.Now);
                        objParameter[6] = new MySqlParameter("LastModifiedBy", DBNull.Value);
                        objParameter[7] = new MySqlParameter("LastModifiedOn", DateTime.Now);
                        objParameter[8] = new MySqlParameter("TotalAvailability", TotalAvailabilty * 1024);
                        objParameter[9] = new MySqlParameter("ShareToDeviceId", ShareToDeviceId);

                        SQLHelper.ExecuteNonQuery("SP_UpdateShareResource", objParameter, oTransaction, oConnection);
                        bool result;
                        using (InvokeShellScripts shellscripts = new InvokeShellScripts())
                        {
                            result = shellscripts.RunShellScriptCommand(true, "Processing", HashId, RAMShared * 1024);
                        }
                        if (result == true)
                        {
                            oTransaction.Commit();
                            return true;
                        }
                        else
                        {
                            oTransaction.Rollback();
                            return false;
                        }

                    }
                    catch (Exception e)
                    {
                        oTransaction.Rollback();
                        Logger.Error(e, "ShareProcessingUpdate Method");
                        return false;
                    }

                }
            }


        }

        public bool ShareStorageUpdate(long DeviceId, decimal TotalAvailabilty, decimal StorageShared, string Username, long ShareToDeviceId)
        {

            using (MySqlConnection oConnection = new MySqlConnection(Utility.GetConnectionString()))
            {
                oConnection.Open();
                using (MySqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    try
                    {
                        string HashId = CommonClass.DB.Devicedetails.Where(x => x.Id == DeviceId).Select(x => x.HashId).FirstOrDefault();
                        //string ConnectionString = Utility.GetConnectionString();
                        MySqlParameter[] objParameter = new MySqlParameter[10];
                        objParameter[0] = new MySqlParameter("DeviceId", DeviceId);
                        objParameter[1] = new MySqlParameter("ResourceType", 2);
                        objParameter[2] = new MySqlParameter("ProcessingShared", DBNull.Value);
                        objParameter[3] = new MySqlParameter("StorageShared", StorageShared * 1024);
                        objParameter[4] = new MySqlParameter("CreatedBy", Username);
                        objParameter[5] = new MySqlParameter("CreatedOn", DateTime.Now);
                        objParameter[6] = new MySqlParameter("LastModifiedBy", DBNull.Value);
                        objParameter[7] = new MySqlParameter("LastModifiedOn", DateTime.Now);
                        objParameter[8] = new MySqlParameter("TotalAvailability", TotalAvailabilty * 1024);
                        objParameter[9] = new MySqlParameter("ShareToDeviceId", ShareToDeviceId);

                        SQLHelper.ExecuteNonQuery("SP_UpdateShareResource", objParameter, oTransaction, oConnection);
                        bool result;
                        using (InvokeShellScripts shellscripts = new InvokeShellScripts())
                        {
                            result = shellscripts.RunShellScriptCommand(true, "Storage", HashId, StorageShared * 1024);
                        }
                        if (result == true)
                        {
                            oTransaction.Commit();
                            return true;
                        }
                        else
                        {
                            oTransaction.Rollback();
                            return false;
                        }
                    }
                    catch (Exception e)
                    {
                        oTransaction.Rollback();
                        Logger.Error(e, "ShareStorageUpdate Method");
                        return false;
                    }
                }
            }


        }

        public bool UseResProcessingUpdate(string DeviceName, decimal RAMRequired, TimeSpan RequiredTime, string Username)
        {

            long DeviceId = CommonClass.DB.Devicedetails.Where(x => x.DeviceName == DeviceName).Select(x => x.Id).FirstOrDefault();
            string ConnectionString = Utility.GetConnectionString();
            MySqlParameter[] objParameter = new MySqlParameter[7];
            objParameter[0] = new MySqlParameter("DeviceId", DeviceId);
            objParameter[1] = new MySqlParameter("ResourceType", 1);
            objParameter[2] = new MySqlParameter("RAMRequired", RAMRequired);
            objParameter[3] = new MySqlParameter("StorageRequired", DBNull.Value);
            objParameter[4] = new MySqlParameter("CreatedBy", Username);
            objParameter[5] = new MySqlParameter("CreatedOn", DateTime.Now);
            objParameter[6] = new MySqlParameter("RequiredTime", RequiredTime);

            SQLHelper.ExecuteNonQuery("SP_UpdateUseResource", objParameter, ConnectionString);
            return true;
        }

        public decimal? GetAvailableResource(long DeviceId, string ResourceType)
        {
            try
            {
                decimal? TotalAvailibity = 0;
                //long DeviceId = CommonClass.DB.Devicedetails.Where(x => x.DeviceName == DeviceName).Select(x => x.Id).FirstOrDefault();
                if (ResourceType == "RAM")
                {
                    TotalAvailibity = CommonClass.DB.Devicedetails.Where(x => x.Id == DeviceId).Select(x => x.Processing).FirstOrDefault();
                }
                else if (ResourceType == "Storage")
                {
                    TotalAvailibity = CommonClass.DB.Devicedetails.Where(x => x.Id == DeviceId).Select(x => x.Storage).FirstOrDefault();
                    decimal? SharedResource = CommonClass.DB.Shareresourcedetails.Where(x => x.DeviceId == DeviceId && x.IsShareStopped != true).Sum(x => x.StorageShared);

                    TotalAvailibity = decimal.Round((Convert.ToDecimal(TotalAvailibity) - Convert.ToDecimal(SharedResource)) / 1024, 3, MidpointRounding.AwayFromZero);
                }
                return Convert.ToDecimal(TotalAvailibity);
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetAvailableResource Method");
                throw e;
            }
        }
        public decimal? GetAvailableRAM(long DeviceId)
        {
            var Devicedetails = CommonClass.DB.Devicedetails.Where(x => x.Id == DeviceId).FirstOrDefault();
            //decimal? UseResourceCount = 0;
            decimal? ShareResourcevValue = CommonClass.DB.Shareresourcedetails.Where(x => x.DeviceId == DeviceId && x.IsShareStopped != true).Select(x => x.ProcessingShared).Sum();
            ShareResourcevValue = ShareResourcevValue == null ? 0 : ShareResourcevValue;
            decimal? ShareResourceCount = ShareResourcevValue;

            return decimal.Round((Convert.ToDecimal(Devicedetails.Processing) - Convert.ToDecimal(ShareResourcevValue)) / 1024, 3, MidpointRounding.AwayFromZero);

            //decimal? TotalAvailability = 0;
            //List<Useresourcedetails> UseResourcedetails = CommonClass.DB.Useresourcedetails.Where(x => x.DeviceId == DeviceId).ToList();
            //foreach (var item in UseResourcedetails)
            //{
            //    decimal? ResoureCount = 0;
            //    DateTime CreatedOn = item.CreatedOn ?? DateTime.MinValue;
            //    TimeSpan RequiredTime = item.RequiredTime ?? TimeSpan.MinValue;
            //    DateTime TotalTime = CreatedOn.Add(RequiredTime);
            //    int result = DateTime.Compare(DateTime.Now, TotalTime);
            //    if (result < 0 || result == 0)/* active*/
            //    {
            //        ResoureCount = item.Ramrequired == null ? 0 : item.Ramrequired;
            //        UseResourceCount = UseResourceCount + ResoureCount;
            //    }
            //}
            //TotalAvailability = (ShareResourceCount == null ? 0 : ShareResourceCount) - (UseResourceCount == null ? 0 : UseResourceCount);
            //return TotalAvailability;
        }

        public bool UseResStorageUpdate(string DeviceName, decimal StorageRequired, TimeSpan RequiredTime, string Username)
        {

            long DeviceId = CommonClass.DB.Devicedetails.Where(x => x.DeviceName == DeviceName).Select(x => x.Id).FirstOrDefault();
            string ConnectionString = Utility.GetConnectionString();
            MySqlParameter[] objParameter = new MySqlParameter[7];
            objParameter[0] = new MySqlParameter("DeviceId", DeviceId);
            objParameter[1] = new MySqlParameter("ResourceType", 2);
            objParameter[2] = new MySqlParameter("RAMRequired", DBNull.Value);
            objParameter[3] = new MySqlParameter("StorageRequired", StorageRequired);
            objParameter[4] = new MySqlParameter("CreatedBy", Username);
            objParameter[5] = new MySqlParameter("CreatedOn", DateTime.Now);
            objParameter[6] = new MySqlParameter("RequiredTime", RequiredTime);

            SQLHelper.ExecuteNonQuery("SP_UpdateUseResource", objParameter, ConnectionString);
            return true;
        }

        public decimal? GetAvailableStorage(long DeviceId)
        {
            var Devicedetails = CommonClass.DB.Devicedetails.Where(x => x.Id == DeviceId).FirstOrDefault();
            //decimal? UseResourceCount = 0;
            decimal? ShareResourceCount = CommonClass.DB.Shareresourcedetails.Where(x => x.DeviceId == DeviceId && x.IsShareStopped != true).Select(x => x.StorageShared).Sum();
            return decimal.Round((Convert.ToDecimal(Devicedetails.Storage) - Convert.ToDecimal(ShareResourceCount)) / 1024, 3, MidpointRounding.AwayFromZero);
            //decimal? TotalAvailability = 0;
            //List<Useresourcedetails> UseResourcedetails = CommonClass.DB.Useresourcedetails.Where(x => x.DeviceId == DeviceId).ToList();
            //foreach (var item in UseResourcedetails)
            //{
            //    decimal? ResoureCount = 0;
            //    DateTime CreatedOn = item.CreatedOn ?? DateTime.MinValue;
            //    TimeSpan RequiredTime = item.RequiredTime ?? TimeSpan.MinValue;
            //    DateTime TotalTime = CreatedOn.Add(RequiredTime);
            //    int result = DateTime.Compare(DateTime.Now, TotalTime);
            //    if (result < 0 || result == 0)/* active*/
            //    {
            //        ResoureCount = item.StorageRequired == null ? 0 : item.StorageRequired;
            //        UseResourceCount = UseResourceCount + ResoureCount;
            //    }
            //}
            //TotalAvailability = (ShareResourceCount == null ? 0 : ShareResourceCount) - (UseResourceCount == null ? 0 : UseResourceCount);
            //return TotalAvailability;
        }

        public decimal? GetRAMUtilizedResource(long DeviceId)
        {
            try
            {
                //long DeviceId = CommonClass.DB.Devicedetails.Where(x => x.DeviceName == DeviceName).Select(x => x.Id).FirstOrDefault();
                return decimal.Round(Convert.ToDecimal(CommonClass.DB.Shareresourcedetails.Where(x => x.DeviceId == DeviceId && x.IsShareStopped != true).Select(x => x.ProcessingShared).Sum()) / 1024, 3, MidpointRounding.AwayFromZero);
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetRAMUtilizedResourc eMethod");
                throw e;
            }
        }

        public decimal? GetStorageUtilizedResource(long DeviceId)
        {
            try
            {
                // long DeviceId = CommonClass.DB.Devicedetails.Where(x => x.DeviceName == DeviceName).Select(x => x.Id).FirstOrDefault();
                return decimal.Round(Convert.ToDecimal(CommonClass.DB.Shareresourcedetails.Where(x => x.DeviceId == DeviceId && x.IsShareStopped != true).Select(x => x.StorageShared).Sum()) / 1024, 3, MidpointRounding.AwayFromZero);
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetStorageUtilizedResource Method");
                throw e;
            }
        }

        public bool StopResourceSharing(long ShareResourceId, string UserName)
        {

            Shareresourcedetails details = CommonClass.DB.Shareresourcedetails.Where(x => x.Id == ShareResourceId).FirstOrDefault();

            using (dappContext context = new dappContext())
            {
                using (IDbContextTransaction Tran = context.Database.BeginTransaction())
                {
                    try
                    {
                        List<Shareresourcedetails> detailslst = context.Shareresourcedetails.Where(x => x.DeviceId == details.DeviceId && x.IsShareStopped != true && x.ResourceType == details.ResourceType).ToList();
                        foreach (var item in detailslst)
                        {
                            item.IsShareStopped = true;
                            item.ShareStoppedBy = UserName;
                        }

                        string HashId = CommonClass.DB.Devicedetails.Where(x => x.Id == details.DeviceId).Select(x => x.HashId).FirstOrDefault();

                        bool? result;
                        using (InvokeShellScripts shellscripts = new InvokeShellScripts())
                        {
                            result = shellscripts.RunShellScriptCommand(false, (details.ResourceType == 1) ? "Storage" : "Processing", HashId);
                        }

                        if (result == true)
                        {
                            context.SaveChanges();
                            Tran.Commit();
                            return true;
                        }
                        else
                        {
                            Tran.Rollback();
                            return false;
                        }
                    }
                    catch (Exception e)
                    {
                        Tran.Rollback();
                        Logger.Error(e, "StopResourceSharing Method");
                        throw e;
                    }
                }
            }
        }
    }
}
