﻿using DAPP_Logger;
using DAPP_Model;
using DAPP_Model.ERiskDApp;
using DAPP_Service.Interface;
using Nethereum.Web3;
using System;
using System.Threading.Tasks;

namespace DAPP_Service.Service
{
    public class InvokeSmartContract : IInvokeSmartContract, IDisposable
    {
        bool disposed = false;

        ~InvokeSmartContract()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {

                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }


        public async Task<string> GethashIdForDevice(SmartContractModel model)
        {
            try
            {
                var setting = GetAppsettingTemplate.GetAppsetting();
                string contractAddress = setting.appsetting.contractAddress; 
                string DefaultAccount = setting.appsetting.DefaultAccount;
                string privatekey = setting.appsetting.privatekey;
                long GasPrice = Convert.ToInt64(setting.appsetting.GasPrice);
                long Gaslimit = Convert.ToInt64(setting.appsetting.Gaslimit);

                string contractAbi = "[{'constant':false,'inputs':[{'name':'deviceId','type':'uint256'},{'name':'info','type':'string'}],'name':'eriskInformation','outputs':[{'name':'','type':'bool'}],'payable':false,'stateMutability':'nonpayable','type':'function'}]";

                var web3 = new Web3(setting.appsetting.URL);

                //getting the total number of transactions of your sender address.
                var txCount = await web3.Eth.Transactions.GetTransactionCount.SendRequestAsync(DefaultAccount, Nethereum.RPC.Eth.DTOs.BlockParameter.CreatePending());

                //Getting the Contract using the ABI and sender address
                var contract = web3.Eth.GetContract(contractAbi, contractAddress);
                //gettting the function by name
                var register = contract.GetFunction("eriskInformation");
                //passing the parameters to function and get the transction hash of data
                var transactionHash = register.GetData(model.DeviceId, model.Information);

                //doing the offline transaction signing with the private key 
                var encoded = Web3.OfflineTransactionSigner.SignTransaction(privatekey, contractAddress, 0, txCount.Value, GasPrice, Gaslimit, transactionHash);

                //verify an encoded transaction:
                Web3.OfflineTransactionSigner.GetSenderAddress(encoded);

                var HashId = await web3.Eth.Transactions.SendRawTransaction.SendRequestAsync("0x" + encoded);

                return HashId;

            }
            catch (Exception e)
            {
                Logger.Error(e, "GethashIdForDevice Method");
                return null;
            }
        }

    }
}
