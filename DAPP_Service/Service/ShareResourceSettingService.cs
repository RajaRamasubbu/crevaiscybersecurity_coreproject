﻿using DAPP_Logger;
using DAPP_Model;
using DAPP_Model.Common;
using DAPP_Model.ERiskDApp;
using DAPP_Service.Interface;
using MySql.Data.MySqlClient;
using System;
using System.Linq;

namespace DAPP_Service.Service
{
    public class ShareResourceSettingService : IShareResourceSettingService, IDisposable
    {
        bool disposed = false;
        ~ShareResourceSettingService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {

                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }

        public bool ShareProcessingSettingsUpdate(int TurnOffFor, string DeviceName, TimeSpan DeactiveFrom, TimeSpan DeactiveTo, string Username)
        {
            try
            {

                long DeviceId = CommonClass.DB.Devicedetails.Where(x => x.DeviceName == DeviceName).Select(x => x.Id).FirstOrDefault();
                if (DeviceId == 0)
                {
                    string ConnectionString = Utility.GetConnectionString();
                    MySqlParameter[] objParameter = new MySqlParameter[9];
                    objParameter[0] = new MySqlParameter("TurnOffFor", TurnOffFor);
                    objParameter[1] = new MySqlParameter("DeviceId", DBNull.Value);
                    objParameter[2] = new MySqlParameter("TurnOffFrom", DeactiveFrom);
                    objParameter[3] = new MySqlParameter("TurnOffTo", DeactiveTo);
                    objParameter[4] = new MySqlParameter("IsDeleted", 1);
                    objParameter[5] = new MySqlParameter("CreatedBy", Username);
                    objParameter[6] = new MySqlParameter("CreatedOn", DateTime.Now);
                    objParameter[7] = new MySqlParameter("LastModifiedBy", DBNull.Value);
                    objParameter[8] = new MySqlParameter("LastModifiedOn", DateTime.Now);
                    SQLHelper.ExecuteNonQuery("SP_UpdateShareResourceSettings", objParameter, ConnectionString);
                }
                else
                {
                    string ConnectionString = Utility.GetConnectionString();
                    MySqlParameter[] objParameter = new MySqlParameter[9];
                    objParameter[0] = new MySqlParameter("TurnOffFor", TurnOffFor);
                    objParameter[1] = new MySqlParameter("DeviceId", DeviceId);
                    objParameter[2] = new MySqlParameter("TurnOffFrom", DeactiveFrom);
                    objParameter[3] = new MySqlParameter("TurnOffTo", DeactiveTo);
                    objParameter[4] = new MySqlParameter("IsDeleted", 1);
                    objParameter[5] = new MySqlParameter("CreatedBy", Username);
                    objParameter[6] = new MySqlParameter("CreatedOn", DateTime.Now);
                    objParameter[7] = new MySqlParameter("LastModifiedBy", DBNull.Value);
                    objParameter[8] = new MySqlParameter("LastModifiedOn", DateTime.Now);
                    SQLHelper.ExecuteNonQuery("SP_UpdateShareResourceSettings", objParameter, ConnectionString);
                }
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ShareProcessingSettingsUpdate Method");
                throw e;
            }
        }
    }
}
