﻿using System.Collections.Generic;

namespace DAPP_Service.Service
{
    public static class GlobalValues
    {
        public static List<ThreadDetails> ThreadList = new List<ThreadDetails>();
    }

    public class ThreadDetails
    {
        public string ThreadUserId { get; set; }
    }
}
