﻿using DAPP_Model.ERiskDApp;
using System;
using System.Threading.Tasks;

namespace DAPP_Service.Interface
{
    public interface IInvokeSmartContract : IDisposable
    {
        Task<string> GethashIdForDevice(SmartContractModel model);
    }
}
