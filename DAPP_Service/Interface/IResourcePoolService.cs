﻿using System;

namespace DAPP_Service.Interface
{
    public interface IResourcePoolService : IDisposable
    {
        bool ShareProcessingUpdate(long DeviceId, decimal TotalPower, decimal RAMShared, string Username, long ShareToDeviceId);

        bool ShareStorageUpdate(long DeviceId, decimal TotalAvailabilty, decimal StorageShared, string Username, long ShareToDeviceId);

        bool UseResProcessingUpdate(string DeviceName, decimal RAMRequired, TimeSpan RequiredTime, string Username);

        decimal? GetAvailableRAM(long DeviceId);

        bool UseResStorageUpdate(string DeviceName, decimal StorageRequired, TimeSpan RequiredTime, string Username);

        decimal? GetAvailableStorage(long DeviceId);
    }
}
